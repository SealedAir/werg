<?php
/**
 * @file
 * Installation profile for partner portal.
 */

function enabled_blocks() {
  
  $options = array();
  $options["promoted_slider-block_slider|views|slider"] = "Enable the photo slider on the homepage.";
  $options["promoted_slider-block_2|views|slider"] = "Enable thumbnails for the homepage slider.*";
  $options["2|block|content"] = "Display welcome message on the homepage.";
  $options["partners-block|views|content"] = "Display logo of the partners on the homepage.";
  $options["testimonial_slider-block|views|content"] = "Enable testimonials for the homepage.";
  $options["news-block_1|views|content"] = "Enable News section.";
  $options['import_news|none|none'] = "Enable News importer. **";
  $options["special_events_carousel-block|views|content"] = "Enable Events section.";
  $options['import_events|none|none'] = "Enable Events importer. ***";
  $options["workstreams_enable|none|none"] = "Enable Workstreams section. ****";
  
  $form['content_enabling_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Manage portal features'),
    '#description' => t('Manage the features that are available inside the portal.'),
  );
  $form['content_enabling_form']['content_enabling'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => enabled_blocks_defaultvalues(),
    '#description' => t("* Requires 'Enable the photo slider on the homepage'." . "<br>" . 
    "** Allows news to be imported from SealedAir. Requires 'News section'." . "<br>" .
    "*** Allows events to be imported from SealedAir. Requires 'Events section'." . "<br>" .
    "**** Enables the 'Workstreams section' through which user may review products."),
  );
  $form['submit'][] = array(
    '#type' => 'submit',
    '#value' => 'Continue',
  );
  return $form;
}

function contact_info() {
  $form['contact_info_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Setup footer contact information'),
    '#description' => t('Setup footer contact information consisting of the address and the map pointer coordinates.'),
  );
  $form['contact_info_form']['cont_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Set the title of the block containing the address/contact details.'),
    '#default_value' => 'Contact',
    '#required' => TRUE,
  );
  $form['contact_info_form']['cont_address'] = array(
    '#type' => 'textarea',
    '#title' => t('Address'),
    '#description' => t('Set the address details.'),
    '#required' => TRUE,
  );
  $form['contact_info_form']['cont_phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone'),
    '#description' => t('Set the phone number.'),
  );
  $form['contact_info_form']['cont_fax'] = array(
    '#type' => 'textfield',
    '#title' => t('Fax'),
    '#description' => t('Set the fax number.'),
  );
  $form['contact_info_form']['cont_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#description' => t('Set the email.'),
  );
  $form['contact_info_form']['cont_latitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Latitude'),
    '#description' => t('Set the latitude coordinates in decimal format. Required for the footer map pointer.'),
    '#default_value' => '35.1319332',
    '#required' => TRUE,
  );
  $form['contact_info_form']['cont_longitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Longitude'),
    '#description' => t('Set the longitude coordinates in decimal format. Required for the footer map pointer.'),
    '#default_value' => '-80.9066505',
    '#required' => TRUE,
    '#suffix' => '<div class="description">Use the left click to move the map around in order to find the location
    you wish to set. Click on the map to set the point.</div><div id="map" style="height: 250px"></div><script>
    function initMap() {
      var mapDiv = document.getElementById("map");
      var map = new google.maps.Map(mapDiv, {
        center: {lat: 35.1319332, lng: -80.9066505},
        zoom: 8
      });
      var marker;
      google.maps.event.addListener(map, "click", function(event) {
        var lat = event.latLng.lat();
        var lng = event.latLng.lng();
        if( marker ) {
          marker.setPosition(event.latLng);
        } else {
          marker = new google.maps.Marker({
            position: event.latLng, 
            map: map
          });
        }
        document.getElementById("edit-cont-latitude").value = lat.toFixed(7);
        document.getElementById("edit-cont-longitude").value = lng.toFixed(7);
      });
    }
    </script>
    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzO_gZyX-Z9zN4izJ5WkACW-0DrTMPCHc&callback=initMap">
    </script>',
  );
  $form['submit'][] = array(
    '#type' => 'submit',
    '#value' => st('Continue'),
  );
  return $form;
}

function enabled_blocks_submit($form, $form_state) {
  $options = array(
    "promoted_slider-block_slider|views|slider",
    "promoted_slider-block_2|views|slider",
    "2|block|content",
    "partners-block|views|content",
    "testimonial_slider-block|views|content",
    "news-block_1|views|content",
    "special_events_carousel-block|views|content",
    "workstreams_enable|none|none"
    );
    
    $cts = array(
      'news',
      'event',
      'partner',
      'promoted_slider',
      'testimonial',
    );
    
    $permissions = array(
      'news view access',
      'events view access',
      'partners view access',
      'slider view access',
      'testimonial view access',
    );
  foreach ($options as $key) {
    variable_set($key, 0);
    if($key == 'workstreams_enable|none|none') {
      workstream_toggle(0);
    }
    $details = explode("|", $key);
    db_update('block')
    ->fields(array(
        'status' => 0,
      )
    )
    ->condition('theme', 'seeportals')
    ->condition('delta', $details[0])
    ->condition('module', $details[1])
    ->execute();
  }
  
  foreach ($cts as $ct) {
    enabled_blocks_contenttype_state($ct, 0);
  }
  
  foreach ($permissions as $permission) {
    enabled_blocks_views_state($permission, 0);
  }
  import_toggle('news', 0);
  variable_set('import_news|none|none', 0);
  import_toggle('events', 0);
  variable_set('import_events|none|none', 0);
  $options = array(); 
  foreach ($form_state['values']['content_enabling'] as $option) {
    array_push($options, $option);
    $details = explode("|", $option);
    if((isset($details[1])) && (isset($details[2]))) {
      variable_set($option, 1);
      db_update('block')
      ->fields(array(
          'status' => 1,
          'region' => $details[2],
          )
      )
      ->condition('theme', 'seeportals')
      ->condition('delta', $details[0])
      ->condition('module', $details[1])
      ->execute();
      switch($details[0]) {
        case 'promoted_slider-block_slider': enabled_blocks_contenttype_state('promoted_slider', 1); enabled_blocks_views_state('slider view access', 1); break;
        case 'partners-block': enabled_blocks_contenttype_state('partner', 1); enabled_blocks_views_state('partners view access', 1); break;
        case 'news-block_1': enabled_blocks_contenttype_state('news', 1); enabled_blocks_views_state('news view access', 1); break;
        case 'special_events_carousel-block': enabled_blocks_contenttype_state('event', 1); enabled_blocks_views_state('events view access', 1); break;
        case 'import_news': import_toggle('news', 1); break;
        case 'import_events': import_toggle('events', 1); break;
        case 'workstreams_enable': workstream_toggle(1); break;
        case 'testimonial_slider-block': enabled_blocks_contenttype_state('testimonial', 1); enabled_blocks_views_state('testimonial view access', 1); break;
      }
    }
  }
  $rid_admi = get_role_by_name_workstreams('administrator');
  $rid_supedit = get_role_by_name_workstreams('Super-editor');
  if((in_array('import_news|none|none', $options, TRUE)) || (in_array('import_events|none|none', $options, TRUE))) {
    user_role_change_permissions($rid_admi, array(
      'administer feeds' => 1,
      'access import' => 1,
      'import opml feeds' => 1,
      'clear opml feeds' => 1,
      'unlock opml feeds' => 1,
      'import feed feeds' =>1,
      'clear feed feeds' => 1,
      'unlock feed feeds' => 1,
    ));
    user_role_change_permissions($rid_supedit, array(
      'access import' => 1,
    ));
  } else {
    user_role_change_permissions($rid_admi, array(
      'administer feeds' => 0,
      'access import' => 0,
      'import opml feeds' => 0,
      'clear opml feeds' => 0,
      'unlock opml feeds' => 0,
      'import feed feeds' => 0,
      'clear feed feeds' => 0,
      'unlock feed feeds' => 0,
    ));
    user_role_change_permissions($rid_supedit, array(
      'access import' => 0,
    ));
  }
  drupal_set_message("The settings have been applied.");
}

function import_toggle($import, $state) {
  $rid_admi = get_role_by_name_workstreams('administrator');
  $rid_supedit = get_role_by_name_workstreams('Super-editor');
  
  user_role_change_permissions($rid_admi, array(
    'import ' . $import . '_feed_import feeds' => $state,
    'clear ' . $import . '_feed_import feeds' => $state,
    'unlock ' . $import . '_feed_import feeds' => $state,
  ));
  user_role_change_permissions($rid_supedit, array(
    'import ' . $import . '_feed_import feeds' => $state,
    'clear ' . $import . '_feed_import feeds' => $state,
    'unlock ' . $import . '_feed_import feeds' => $state,
  ));
}

function workstream_toggle($state) {
  if (module_exists('workstreams') == NULL) {
    module_enable(array('workstreams'));
    node_access_rebuild($batch_mode = FALSE);
  }
  $rid_admin = get_role_by_name_workstreams('Workstream admin');
  $rid_editor = get_role_by_name_workstreams('Workstream editor');
  $rid_customer = get_role_by_name_workstreams('Workstream customer');

  $views_status = variable_get('views_defaults', array());
  if($state == '0') {
    $views_status['workstream_tree'] = TRUE;
  }
  if($state == '1') {
    $views_status['workstream_tree'] = FALSE;
  }
  variable_set('views_defaults', $views_status);
  $workstream_tree = taxonomy_vocabulary_machine_name_load('workstream_tree');
  $vid_tree = $workstream_tree->vid;
  $workstream_tags = taxonomy_vocabulary_machine_name_load('workstream_tags');
  $vid_tags = $workstream_tags->vid;
  user_role_change_permissions($rid_admin, array(
    'access administration menu' => $state,
    'administer comments' => $state,
    'administer nodes' => $state,
    'administer taxonomy' => $state,
    'view revisions' => $state,
    'delete revisions' => $state,
    'revert revisions' => $state,
    'edit own comments' => $state,
    'access administration pages' => $state,
    'view all unpublished content' => $state,
    'view own unpublished content' => $state,
    'access content overview' => $state,
    'access workstream settings' => $state,
    'create workstream content' => $state,
    'delete any workstream content' => $state,
    'delete own workstream content' => $state,
    'edit any workstream content' => $state,
    'edit own workstream content' => $state,
    'delete terms in ' . $vid_tags => $state,
    'delete terms in ' . $vid_tree => $state,
    'edit terms in ' . $vid_tags => $state,
    'edit terms in ' . $vid_tree => $state,
    'access workstream settings' => $state,
    'access comments' => $state,
    'post comments' => $state,
    'skip comment approval' => $state,
  ));
  user_role_change_permissions($rid_editor, array(
    'access administration menu' => $state,
    'access administration pages' => $state,
    'access content overview' => $state,
    'create workstream content' => $state,
    'edit any workstream content' => $state,
    'edit own workstream content' => $state,
    'delete own workstream content' => $state,
    'view all unpublished content' => $state,
    'view own unpublished content' => $state,
    'view revisions' => $state,
    'edit own comments' => $state,
    'administer nodes' => $state,
    'access comments' => $state,
    'post comments' => $state,
    'skip comment approval' => $state,
  ));
  user_role_change_permissions($rid_customer, array(
    'edit own comments' => $state,
    'view revisions' => $state,
    'view own unpublished content' => $state,
    'access comments' => $state,
    'post comments' => $state,
    'skip comment approval' => $state,
  ));
  if (function_exists('views_invalidate_cache')) {
    views_invalidate_cache();
  }
  switch($state) {
    case 0:
      $links = menu_load_links('main-menu');
      foreach($links as $link) {
        if($link['link_title'] == 'Workstream') {
          $mlid = $link['mlid'];
          menu_link_delete($mlid, NULL);
        }
      }
      break;
    case 1: 
      drupal_valid_path('workstream-tree');
      $link_stat = array(
        'values' => array(
          'menu_name'  => 'main-menu',
          'weight' => 49,
          'link_title' => 'Workstream',
          'link_path'  => 'workstream-tree',
          ),
      );
      menu_link_save($link_stat['values']); 
      break;
  }
}

function get_role_by_name_workstreams($name) {
  $roles = user_roles();
  return array_search($name, $roles);
}

function enabled_blocks_contenttype_state($ct, $state) {
  $rid_admi = get_role_by_name_workstreams('administrator');
  $rid_edit = get_role_by_name_workstreams('Editor');
  $rid_supedit = get_role_by_name_workstreams('Super-editor');
  
  $roles = array($rid_admi, $rid_edit, $rid_supedit);
  foreach ($roles AS $role) {
    user_role_change_permissions($role, array(
    'create ' . $ct . ' content' => $state,
    'edit own ' . $ct . ' content' => $state,
    'edit any ' . $ct . ' content' => $state,
    'delete own ' . $ct . ' content' => $state,
    'delete any ' . $ct . ' content' => $state,
    ));
  }
}

function enabled_blocks_views_state($permission, $state) {
  $rid_admi = get_role_by_name_workstreams('administrator');
  $rid_edit = get_role_by_name_workstreams('Editor');
  $rid_supedit = get_role_by_name_workstreams('Super-editor');
  $rid_anon = get_role_by_name_workstreams('anonymous user');
  $rid_loged = get_role_by_name_workstreams('authenticated user');
  
  $roles = array($rid_admi, $rid_edit, $rid_supedit, $rid_loged, $rid_anon);
  foreach ($roles AS $role) {
    user_role_change_permissions($role, array(
    $permission => $state,
    ));
  }
}



function enabled_blocks_defaultvalues() {
  $options = array(
    "promoted_slider-block_slider|views|slider",
    "promoted_slider-block_2|views|slider",
    "2|block|content",
    "partners-block|views|content",
    "testimonial_slider-block|views|content",
    "news-block_1|views|content",
    "special_events_carousel-block|views|content",
    "workstreams_enable|none|none",
    "import_news|none|none",
    "import_events|none|none"
    );
    $option_list = array();
    foreach ($options as $key) {
      if(variable_get($key) != 0)
        $option_list[$key] = $key;
    }
  return $option_list;
}

function enabled_blocks_validate($form, &$form_state) {
  $options = array();
  foreach ($form_state['values']['content_enabling'] as $option) {
    array_push($options, $option);
  }
  if(in_array('promoted_slider-block_2|views|slider', $options, TRUE)) {
    if($options[0] == 'promoted_slider-block_2|views|slider' ) {
      form_set_error('promoted_slider-block_slider|views|slider', '"Main page slider" needs to be selected in order to use the "Main page slider thumbnails".');
    }
  }
  if(in_array('import_news|none|none', $options, TRUE) && !in_array('news-block_1|views|content', $options, TRUE)) {
    form_set_error('news-block_1|views|content' , '"News Section" needs to be selected in order to use the "News Importer".');
  }
  if(in_array('import_events|none|none', $options, TRUE) && !in_array('special_events_carousel-block|views|content', $options, TRUE)) {
    form_set_error('special_events_carousel-block|views|content' , '"Events Section" needs to be selected in order to use the "Events Importer".');
  }
}

function contact_info_validate($form, $form_state) {
  $phone = $form_state['values']['cont_phone'];
  $fax = $form_state['values']['cont_fax'];
  $email = $form_state['values']['cont_email'];
  $lat = $form_state['values']['cont_latitude'];
  $long = $form_state['values']['cont_longitude'];

  if(preg_match('/^([0-9\(\)\/\+ \-]*)$/', $phone, $match_phone) != 1) {
    form_set_error('cont_phone', 'Insert valid phone number.');
  }
  if(preg_match('/^([0-9\(\)\/\+ \-]*)$/', $fax, $match_fax) != 1) {
    form_set_error('cont_fax', 'Insert valid fax number.');
  }
  if((preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $email, $match_email) != 1) && ($email != '')) {
    form_set_error('cont_email', 'Insert valid email.');
  }
  if(preg_match('/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,7})?))$/', $lat, $matchlat) != 1) {
    form_set_error('cont_latitude', 'Insert valid latitude coordinates (-90 to 90 with max 7 decimals).');
  }
  if(preg_match('/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[1-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,7})?))$/', $long, $matchlong) != 1) {
    form_set_error('cont_longitude', 'Insert valid longitude coordinates (-180 to 180 with max 7 decimals).');
  }
}

function contact_info_submit($form, $form_state) {
  $node = new stdClass();
  $node->type = "contact";
  node_object_prepare($node);
  $node->language = 'en';
  $node->uid = 1; 
  $node->status = 1;
  $node->promote = 0;
  $node->comment = 0;
  $node->title = $form_state['values']['cont_title'];
  $node->field_adress = array('und' => array('0' => array('value' => $form_state['values']['cont_address'], 'format' => 'full_html')));
  $node->field_phone = array('und' => array('0' => array('value' => $form_state['values']['cont_phone'])));
  $node->field_fax = array('und' => array('0' => array('value' => $form_state['values']['cont_fax'])));
  $node->field_email = array('und' => array('0' => array('email' => $form_state['values']['cont_email'])));
  $node->field_map_latitude = array('und' => array('0' => array('value' => $form_state['values']['cont_latitude'])));
  $node->field_map_longitude = array('und' => array('0' => array('value' => $form_state['values']['cont_longitude'])));
  $node = node_submit($node);
  node_save($node);
  module_enable(array('partner_portal_flexslider_thumbnail'));
  block_access_permissions();
}

function block_access_permissions() {
  $variables = array (
    "block|2|3|config",
    "block|2|3|config_body",
    "block|2|3|config_desc",
    "block|2|3|config_lang",
    "block|2|3|config_page_visibility",
    "block|2|3|config_region",
    "block|2|3|config_role_visibility",
    "block|2|3|config_title",
    "block|2|3|config_user_visibility",
    "block|2|3|disable",
    "block|2|3|enable",
    "block|2|3|move",
    "block|2|3|view",
    "block|2|4|config_body",
    "block|2|4|config_title",
    "block|2|5|config_body",
    "block|2|5|config_title",
    "block|3|5|config_body",
  );

  foreach ($variables as $variable) {
    $var = explode("|", $variable);
    $record = array(
      'rid' => $var[2],
      'module' => $var[0],
      'delta' => $var[1],
      'permission' => $var[3]);
    $result = drupal_write_record('block_access_roles', $record);
  }
  
  $import_menu = array(
    'values' => array(
      'menu_name'  => 'management',
      'weight'     => 1,
      'link_title' => 'Import',
      'link_path'  => 'import',
     ),
  );
  menu_link_save($import_menu['values']);
  
  $see_menu = array(
    'values' => array(
      'menu_name'  => 'management',
      'weight'     => 1,
      'link_title' => 'SEEMenu',
      'link_path'  => 'seemenu',
     ),
  );
  menu_link_save($see_menu['values']);
}