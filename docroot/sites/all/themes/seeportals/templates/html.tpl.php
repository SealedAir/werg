<?php
/**
 * @file
 * Default theme implementation to display the basic html structure of a single.
 *
 * Drupal page.
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, used in the TITLE tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   That were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for HEAD section (including meta tags, keyword tags, etc).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings.
 * - $page_top: Initial markup from any modules that have altered the
 *   Page. It should always be output first, before all other dynamic content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. It should always be output last, after all other dynamic content.
 * - $classes String of classes that can be used to style contextually in CSS.
 *
 * @see bootstrap_preprocess_html()
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 *
 * @ingroup themeable
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces;?>>
<head profile="<?php print $grddl_profile; ?>">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
<link rel="apple-touch-icon" sizes="57x57" href="/<?php print path_to_theme(); ?>/images/favicons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="114x114" href="/<?php print path_to_theme(); ?>/images/favicons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="72x72" href="/<?php print path_to_theme(); ?>/images/favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="144x144" href="/<?php print path_to_theme(); ?>/images/favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="60x60" href="/<?php print path_to_theme(); ?>/images/favicons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="120x120" href="/<?php print path_to_theme(); ?>/images/favicons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="76x76" href="/<?php print path_to_theme(); ?>/images/favicons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="152x152" href="/<?php print path_to_theme(); ?>/images/favicons/apple-touch-icon-152x152.png">
<link rel="icon" type="image/png" href="/<?php print path_to_theme(); ?>/images/favicons/favicon-196x196.png" sizes="196x196">
<link rel="icon" type="image/png" href="/<?php print path_to_theme(); ?>/images/favicons/favicon-160x160.png" sizes="160x160">
<link rel="icon" type="image/png" href="/<?php print path_to_theme(); ?>/images/favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/<?php print path_to_theme(); ?>/images/favicons/favicon-16x16.png" sizes="16x16">
<link rel="icon" type="image/png" href="/<?php print path_to_theme(); ?>/images/favicons/favicon-32x32.png" sizes="32x32">
<meta name="msapplication-TileColor" content="#008998">
<meta name="msapplication-TileImage" content="<?php print path_to_theme(); ?>/images/favicons/mstile-144x144.png">
  <?php print $styles; ?>
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <?php print $scripts; ?>

</head>
<body data-spy="scroll" data-target=".navbar-collapse" data-offset="100" class="<?php print $classes; ?>" <?php print $attributes;?>>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-K9PJTV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K9PJTV');</script>
<!-- End Google Tag Manager -->
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
