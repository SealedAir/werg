<?php
/**
 * @file
 * Node template a.
 */
?>
<div class="template-b-content">
<div class="header-description-b">
<?php
  if(isset($node->field_header_image_b['und'][0]['file']->uri)) {
    $header_url = file_create_url($node->field_header_image_b['und'][0]['file']->uri);
    print theme('image', array('path' => $header_url, 'alt' => ' '));
  }
  if(isset($node->field_description_b['und'][0]['value'])) {
    $description_b = $node->field_description_b['und'][0]['value'];
    print media_wysiwyg_filter($description_b);
  }
?>
</div>

<div class="tab-content">
  <?php
    foreach ($node->field_subsection_template_b['und'] as $key => $value) {
      $subsection = field_collection_item_load($value['value'], $reset = FALSE);
        print '<div class="" id="' . $key . '">';
        print '<div class="paragraph-region">';
          if(isset($subsection->field_subsection_title_b['und'][0]['value'])) {
            $subsection_title = $subsection->field_subsection_title_b['und'][0]['value'];
            print '<h2>' . $subsection_title . '</h2>';
          }
          foreach ($subsection->field_paragraph_subsection_b['und'] as $key => $value) {
            $paragraph = field_collection_item_load($value['value'], $reset = FALSE);
            $style_class = $paragraph->field_paragraph_style_b['und'][0]['taxonomy_term']->field_class['und'][0]['value'];
            print '<div class="' . $style_class . '">';
            if($style_class == 'image-full') {
              print '<div class="paragraph-content">';
              if(isset($paragraph->field_paragraph_text_b['und'][0]['value'])) {
                $paragraph_text = $paragraph->field_paragraph_text_b['und'][0]['value'];
                print media_wysiwyg_filter($paragraph_text);
              }
              if(isset($paragraph->field_file_attachements['und'])) {
                foreach($paragraph->field_file_attachements['und'] as $key => $value) {
                  print theme('file_link', array('file' => $value['file']));
                }
              }
              print '</div>';
              if(isset($paragraph->field_paragraph_image_b['und'][0]['file']->uri)) {
                $img_uri = $paragraph->field_paragraph_image_b['und'][0]['file']->uri;
                $img = file_create_url($img_uri);
                if($style_class == 'image-full') {
                  print theme('image', array('path' => $img, 'alt' => ' '));
                }
                else {
                  print theme('image_style', array('path' => $img_uri, 'style_name' => 'image_description_template'));
                }
              }
            }
            else {
              if(isset($paragraph->field_paragraph_image_b['und'][0]['file']->uri)) {
                $img_uri = $paragraph->field_paragraph_image_b['und'][0]['file']->uri;
                $img = file_create_url($img_uri);
                if($style_class == 'image-full') {
                  print theme('image', array('path' => $img, 'alt' => ' '));
                }
                else {
                    print theme('image_style', array('path' => $img_uri, 'style_name' => 'image_description_template'));
                }
              }
              print '<div class="paragraph-content">';
              if(isset($paragraph->field_paragraph_text_b['und'][0]['value'])) {
                $paragraph_text = $paragraph->field_paragraph_text_b['und'][0]['value'];
                print media_wysiwyg_filter($paragraph_text);
              }
              if(isset($paragraph->field_file_attachements['und'])) {
                foreach($paragraph->field_file_attachements['und'] as $key => $value) {
                 print theme('file_link', array('file' => $value['file']));
                }
              }
              print '</div>';
            }
            print '</div>';
          }
        print '</div>';
      print '</div>';
    }
    if (isset($node->field_blocks_subsection_b['und'])) {
      print '<div class="blocks-region">';
      foreach ($node->field_blocks_subsection_b['und'] as $key => $value) {
        $blocks = field_collection_item_load($value['value'], $reset = FALSE);
        print '<div class="col-lg-4 col-md-4 col-sm-4 block_template">';
        if(isset($blocks->field_block_title_b['und'][0]['value'])) {
          $block_title = $blocks->field_block_title_b['und'][0]['value'];
          print '<h3>' . $block_title . '</h3>';
        }
        if(isset($blocks->field_block_text_b['und'][0]['value'])) {
          $block_text = $blocks->field_block_text_b['und'][0]['value'];
          print media_wysiwyg_filter($block_text);
        }
        if(isset($blocks->field_file_attachements['und'])) {
          foreach($blocks->field_file_attachements['und'] as $key => $value) {
            print theme('file_link', array('file' => $value['file']));
          }
        }
    if(isset($blocks->field_block_image['und'][0]['uri'])) {
          foreach($blocks->field_block_image['und'] as $key => $value) {
    $img_uri = $blocks->field_block_image['und'][0]['uri'];
            $img = file_create_url($img_uri);
            print theme('image', array('path' => $img, 'alt' => ' '));
          }
        }
        print '</div>';
      }
      print '</div>';
    }
  ?>
</div>
</div>
