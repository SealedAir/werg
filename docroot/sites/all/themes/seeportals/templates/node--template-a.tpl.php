<?php
/**
 * @file
 * Node template a.
 */
?>
<div class="template-a-content">
  <div class="header-image">
  <?php
    if(isset($node->field_header_image_a['und'][0]['file']->uri)) {
    $header_url = file_create_url($node->field_header_image_a['und'][0]['file']->uri);
    print theme('image', array('path' => $header_url, 'alt' => ' '));
    }
  ?>
  </div>
<?php if (isset($node->field_subsection_template_a['und'][1]['value'])): ?>
  <ul class="template-a-tabs nav nav-tabs" role="tablist" id="template_tab">
    <?php
     foreach ($node->field_subsection_template_a['und'] as $key => $value) {
        $subsection = field_collection_item_load($value['value'], $reset = FALSE);
        if(isset($subsection->field_subsection_title_a['und'][0]['value'])) {
          $subsection_title = $subsection->field_subsection_title_a['und'][0]['value'];
          if($key == 0) {
            print '<li  class="active" ><a href="#' . $key . '"  aria-controls="' . $key . '" role="tab" data-toggle="tab" >' . $subsection_title . '</a></li>';
          }
          else {
            print '<li  class="" ><a href="#' . $key . '" aria-controls="' . $key . '" role="tab" data-toggle="tab" >' . $subsection_title . '</a></li>';
          }
        }
      }

    ?>
    <li style="height: 15px; width:1px; display: inline-block;"></li>
 </ul>
<?php endif; ?>


  <div class="tab-content">
    <?php
      foreach ($node->field_subsection_template_a['und'] as $key => $value) {
        $subsection = field_collection_item_load($value['value'], $reset = FALSE);
        if($key == 0){
          print '<div class="tab-pane active" id="' . $key . '">';
        }
        else {
          print '<div class="tab-pane" id="' . $key . '">';
        }
          print '<div class="col-sm-9 col-sm-push-3 right-region">';
            if(isset($subsection->field_subsection_title_a['und'][0]['value'])) {
              $subsection_title = $subsection->field_subsection_title_a['und'][0]['value'];
              print '<h2>' . $subsection_title . '</h2>';
            }
            foreach ($subsection->field_paragraph_subsection_a['und'] as $key => $value) {
              $paragraph = field_collection_item_load($value['value'], $reset = FALSE);
              $style_class = $paragraph->field_paragraph_style_a['und'][0]['taxonomy_term']->field_class['und'][0]['value'];
              print '<div class="' . $style_class . '">';
              if($style_class == 'image-full') {
                print '<div class="paragraph-content">';
                if(isset($paragraph->field_paragraph_text_a['und'][0]['value'])) {
                  $paragraph_text = $paragraph->field_paragraph_text_a['und'][0]['value'];
                  print media_wysiwyg_filter($paragraph_text);
                }
                if(isset($paragraph->field_file_attachements['und'])) {
                  foreach($paragraph->field_file_attachements['und'] as $key => $value) {
                    print theme('file_link', array('file' => $value['file']));
                  }
                }
                print '</div>';
                if(isset($paragraph->field_paragraph_image_a['und'][0]['file']->uri)) {
                  $img_uri = $paragraph->field_paragraph_image_a['und'][0]['file']->uri;
                  $img = file_create_url($img_uri);
                  if($style_class == 'image-full') {
                    print theme('image', array('path' => $img, 'alt' => ' '));
                  }
                  else {
                    print theme('image_style', array('path' => $img_uri, 'style_name' => 'image_description_template'));
                  }
                }
              }
              else {
                if(isset($paragraph->field_paragraph_image_a['und'][0]['file']->uri)) {
                  $img_uri = $paragraph->field_paragraph_image_a['und'][0]['file']->uri;
                  $img = file_create_url($img_uri);
                  if($style_class == 'image-full') {
                    print theme('image', array('path' => $img, 'alt' => ' '));
                  }
                  else {
                      print theme('image_style', array('path' => $img_uri, 'style_name' => 'image_description_template'));
                  }
                }
                print '<div class="paragraph-content">';
                if(isset($paragraph->field_paragraph_text_a['und'][0]['value'])) {
                  $paragraph_text = $paragraph->field_paragraph_text_a['und'][0]['value'];
                  print media_wysiwyg_filter($paragraph_text);
                }
                if(isset($paragraph->field_file_attachements['und'])) {
                  foreach($paragraph->field_file_attachements['und'] as $key => $value) {
                   print theme('file_link', array('file' => $value['file']));
                  }
                }
                print '</div>';
              }
              print '</div>';
            }
          print '</div>';
          print '<div class="col-sm-3 col-sm-pull-9 left-blocks">';
           if (isset($subsection->field_blocks_subsection_a['und'])) {
             foreach ($subsection->field_blocks_subsection_a['und'] as $key => $value) {
                $blocks = field_collection_item_load($value['value'], $reset = FALSE);
                print '<div class="block_template">';
               if(isset($blocks->field_block_title_a['und'][0]['value'])) {
                  $block_title = $blocks->field_block_title_a['und'][0]['value'];
                  print '<h3>' . $block_title . '</h3>';

               }
                if(isset($blocks->field_block_text_a['und'][0]['value'])) {
                  $block_text = $blocks->field_block_text_a['und'][0]['value'];
                  print media_wysiwyg_filter($block_text);
                }
                if(isset($blocks->field_file_attachements['und'])) {
                  foreach($blocks->field_file_attachements['und'] as $key => $value) {
                    print theme('file_link', array('file' => $value['file']));
                  }
                }
                print '</div>';
             }
           }
          print '</div>';
        print '</div>';
      }
    ?>
  </div>

  <div class="footer-image">
  <?php
    if(isset($node->field_footer_image_a['und'][0]['file']->uri)) {
      $footer_uri = $node->field_footer_image_a['und'][0]['file']->uri;
      print theme('image_style', array('path' => $footer_uri, 'style_name' => 'footer_image_template'));
    }
  ?>
  </div>
</div>
