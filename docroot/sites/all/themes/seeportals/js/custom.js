/**
 * @file
 * Custom js.
 */

function delete_slash() {
  jQuery(".menu.nav a[href*='/#']").each(function() {
        str = jQuery(this).attr("href");
        str = str.substring(1);
        jQuery(this).attr("href", str);
  });
}

function toggleMobileMenu(el) {
  jQuery(el).closest("ul").toggleClass("open");
}
jQuery(document).ready(function() {
  jQuery('.block-search button').removeClass('element-invisible btn-primary btn').addClass('search-submit');
  var window_width = jQuery(window).width();
  if ((window_width >= 1200 && window_width < 1300) || (window_width < 1300 && window_width >= 1200)) {
    jQuery('.slick__slide').css('width', (window_width - 128) / 3);
  }
  // Set the width of the 3 slick slide items for the 1000px - 1200px interval.
  if ((window_width >= 1000 && window_width < 1200) || (window_width < 1200 && window_width >= 1000)) {
    jQuery('.slick__slide').css('width', (window_width - 124) / 3);
  }
   // Set the width of the 2 slick slide items for the 600px - 1000px interval.
  if ((window_width >= 600 && window_width < 1000) || (window_width < 1000 && window_width >= 600)) {
    jQuery('.slick__slide').css('width', (window_width - 92) / 2);
  }
  var slick_slider_image_height = jQuery('.events-carousel-block .views-field-field-image-node').height();
  jQuery('.events-carousel-block .views-field-title').css('height', slick_slider_image_height);
  jQuery('.events-carousel-block .views-field-field-special-date').css('height', slick_slider_image_height);
  var url = window.location.pathname.substring(1);
  jQuery("#edit-dynamic-field-wrapper").addClass('modal');
  // Autosubmit form filters on Pricing dashboard.
  jQuery('#views-exposed-form-Simulation-price-breakup-page-1 #edit-title').bind('input', function(){
    jQuery('#views-exposed-form-Simulation-price-breakup-page-1').submit();
  });
  jQuery('#views-exposed-form-Simulation-price-breakup-page-1 #edit-field-description-value').bind('input', function(){
    jQuery('#views-exposed-form-Simulation-price-breakup-page-1').submit();
  });
  jQuery('#views-exposed-form-Simulation-price-breakup-page-1 #edit-field-pricing-date-value-value-datepicker-popup-0').bind('input', function(){
    jQuery('#views-exposed-form-Simulation-price-breakup-page-1').submit();
  });
  jQuery('#views-exposed-form-Simulation-price-breakup-page-1 #edit-field-pricing-date-value-value-datepicker-popup-0').change(function(){
    jQuery('#views-exposed-form-Simulation-price-breakup-page-1').submit();
  });
  jQuery('#views-exposed-form-Simulation-price-breakup-page-1 #btn-close-modal').click(function(){
    jQuery('#views-exposed-form-Simulation-price-breakup-page-1').submit();
  });
  // A var a = url.split('/').reverse()[0];
  if (url == 'cart') {
    if (window.localStorage) {
      if (!localStorage.getItem('firstLoad')) {
        localStorage['firstLoad'] = true;
        window.location.reload();
      }
      else {
        localStorage.removeItem('firstLoad');
      }
    }
  }
  jQuery("#edit-state-of-residence").change(function(){
    var id = jQuery(this).val();
    var baseUrl = document.location.origin;
    jQuery.ajax({ url : baseUrl + '/distloc', data : {id : id}, success: function(data) {
      jQuery("#edit-panes-delivery-delivery-first-name").val(data.title);
      jQuery("#edit-panes-delivery-delivery-last-name").val(data.body);
      jQuery("#edit-panes-delivery-delivery-city").val(data.city);
      jQuery("#edit-panes-delivery-delivery-postal-code").val(data.postal_code);
      jQuery("#edit-panes-delivery-delivery-street1").val(data.thorough_fare);
      jQuery("#edit-panes-delivery-delivery-street2").val(data.street2);
      jQuery("#edit-panes-delivery-delivery-country").val(840);
      jQuery("#edit-panes-delivery-delivery-zone").val(data.state);
      jQuery("#edit-panes-delivery-delivery-company").val(data.locname);
    }});
  });
  // Hide editors to node access form.
  jQuery('#field-node-access-values > tbody  > tr').each(function(e) {
    if (jQuery('#node_access_editor', jQuery(this)).length) {
      jQuery(this).addClass('hidden');
    }
  });
  // Close menu responsive.
  jQuery('.navbar-collapse li a').click(function(e) {
    if (!jQuery(this).parent().hasClass('expanded')) {
      jQuery('.navbar-collapse').removeClass("in");
      jQuery('.navbar-collapse').addClass("collapse");
    }
  });
  var window_width = jQuery(window).width();
  // Responsive tabs.
  jQuery("ul.template-a-tabs li:not('.active')").click(function() {
    jQuery('li.active').removeClass("active");
    jQuery(this).parent().addClass("active");
    toggleMobileMenu(this);
  })
  jQuery('ul.template-a-tabs > li.active a').click(function() {
    toggleMobileMenu(this);
  });
  var window_width = jQuery(window).width();
  if (jQuery('#template_tab').length) {
    var height_nav_tabs = jQuery('#template_tab').height() + 60;
  }
  if (window_width > 767) {
    jQuery('.right-region').css('margin-top', -height_nav_tabs);
  }
  else {
    jQuery('.right-region').css('margin-top', '0');
    jQuery('.template-b-tabs').click(function() {
      jQuery('.template-b-tabs li').toggleClass('show');
    })
  }
  var height_section_fixed = jQuery('.section-fixed').height();
  jQuery('.background-slider').css('height', height_section_fixed);
  // Page scroll.
  if (jQuery('body').hasClass('front')) {
    delete_slash();
  }
  if (window.location.hash) {
    setTimeout(function() {
      jQuery('html, body').scrollTop(0).show();
      jQuery('html, body').animate({
        scrollTop: jQuery(window.location.hash).offset().top
      }, 1500, 'easeInOutExpo')
    }, 0);
  }
  else {
    jQuery('html, body').show();
  }
  jQuery('body').on('click', '.page-scroll a', function(event) {
    var $anchor = jQuery(this);
    jQuery('html, body').stop().animate({
      scrollTop: jQuery($anchor.attr('href')).offset().top - 100
    }, 1500, 'easeInOutExpo');
    event.preventDefault();
  });
  // Show hide element.
  jQuery('.toggle').click(function(event) {
    event.preventDefault();
    var target = jQuery(this).attr('href');
    jQuery(target).toggleClass('hidden show');
  });
  // Fix background fixed bug.
  if (jQuery('.section-content').length) {
    var background_active = jQuery('.section-content').offset().top;
  }
  if (jQuery('.template-b-tabs').length) {
    var bottom = jQuery('.template-b-tabs').offset().top;
    var sticky_div_height = jQuery('.template-b-tabs').height() + 10;
    if (window_width < 750) {
      sticky_div_height = 35;
    }
  }
  jQuery(window).scroll(function() {
    var top = jQuery(this).scrollTop();
    if (jQuery('.front-background-image').length) {
      if (top > background_active) {
        jQuery('.flexslider .slides').addClass('fix-background');
      }
      else {
        jQuery('.flexslider .slides').removeClass('fix-background');
      }
    }
    if (top > bottom) {
      jQuery('.template-b-tabs').addClass('fixed-section-template');
      jQuery('.sticky-div-template-b').css({
        'display': 'block',
        'height': sticky_div_height,
        'overflow': 'visible'
      })
    }
    else {
      jQuery('.template-b-tabs').removeClass('fixed-section-template');
      jQuery('.sticky-div-template-b').css('display', 'none');
    }
  });
  // Search.
  jQuery('.search-link').click(function(e) {
    e.preventDefault();
    jQuery('.block-search').toggle();
    jQuery('.search-box').css('width', '250px');
    if (jQuery('.block-search').is(':not(:visible)')) {
      jQuery('.search-box').css('width', '60px');
      jQuery('#edit-search-block-form--2').css('width', '0px');
    }
    else {
      if (jQuery(window).width() >= 992) {
        jQuery('#edit-search-block-form--2').css('width', '165px');
      }
    }
  });
  jQuery('.header-search .input-group-btn').html('<button type="button" class="close-search">X</button>');
  jQuery('.close-search').click(function() {
    jQuery('.header-search').hide();
  });
});

(function($) {
  Drupal.behaviors.QuicklinksonLeft = {
    attach: function(context, settings) {
      var height = $('.template-a-content .tab-pane:first-child .right-region').height();
      if ($('.template-a-tabs').height() >= height) {
        if ($('.template-a-tabs li:first-child').hasClass('active')) {
          $('.template-a-content .tab-pane.active .left-blocks').addClass('s_left');
        }
      }
      else {
        if (height < 150) {
          $('.template-a-content .tab-pane.active .left-blocks').addClass('s_left');
        }
      }
      $('.template-a-tabs a').click(function() {
        if (!$(this).parent().hasClass('active')) {
          var index = $(this).parent().index() + 1;
          var height = $('.template-a-content .tab-pane:nth-child(' + index + ') .right-region').actual('height');
          if ($('.template-a-tabs').height() >= height) {
            $('.template-a-content .tab-pane .left-blocks').each(function() {
              $(this).removeClass('s_left');
            })
            $('.template-a-content .tab-pane:nth-child(' + index + ') .left-blocks').toggleClass('s_left');
          }
          else {
            if (height < 150) {
              $('.template-a-content .tab-pane .left-blocks').each(function() {
                $(this).removeClass('s_left');
              })
              $('.template-a-content .tab-pane:nth-child(' + index + ') .left-blocks').toggleClass('s_left');
            }
          }
        }
      })
    }
  };
  // Sliders.
  Drupal.behaviors.slick_carousel = {
    attach: function(context, settings) {
      var window_width = jQuery(window).width();
      if ((window_width >= 1200 && window_width < 1300) || (window_width < 1300 && window_width >= 1200)) {
        jQuery('.slick__slide').css('width', (window_width - 12) / 3);
      }
      // Set the width of the 3 slick slide items for the 1000px - 1200px interval.
      if ((window_width >= 1000 && window_width < 1200) || (window_width < 1200 && window_width >= 1000)) {
        jQuery('.slick__slide').css('width', (window_width - 124) / 3);
      }
       // Set the width of the 2 slick slide items for the 600px - 1000px interval.
      if ((window_width >= 600 && window_width < 1000) || (window_width < 1000 && window_width >= 600)) {
        jQuery('.slick__slide').css('width', (window_width - 92) / 2);
      }
      var slick_slider_image_height = jQuery('.slide__content .views-field-field-image-node').height();
      jQuery('.slide__content .views-field-title').css('height', slick_slider_image_height);
      jQuery('.slide__content .views-field-field-special-date').css('height', slick_slider_image_height);
      jQuery(window).resize(function() {
        var height = jQuery('.section-fixed').height();
        var window_width = jQuery(window).width();
        jQuery('.background-slider').css('height', height);
        if ((window_width >= 1200 && window_width < 1300) || (window_width < 1300 && window_width >= 1200)) {
          jQuery('.slick__slide').css('width', (window_width - 128) / 3);
        }
        // Set the width of the 3 slick slide items for the 1000px - 1200px interval.
        if ((window_width >= 1000 && window_width < 1200) || (window_width < 1200 && window_width >= 1000)) {
          jQuery('.slick__slide').css('width', (window_width - 124) / 3);
        }
        // Set the width of the 2 slick slide items for the 600px - 1000px interval.
        if ((window_width >= 600 && window_width < 1000) || (window_width < 1000 && window_width >= 600)) {
          jQuery('.slick__slide').css('width', (window_width - 92) / 2);
        }
        var slick_slider_image_height = jQuery('.slide__content .views-field-field-image-node').height();
        jQuery('.slide__content .views-field-title').css('height', slick_slider_image_height);
        jQuery('.slide__content .views-field-field-special-date').css('height', slick_slider_image_height);
        if (jQuery('#template_tab').length) {
          var height_nav_tabs = jQuery('#template_tab').height() + 60;
        }
        if (window_width > 767) {
          jQuery('.right-region').css('margin-top', -height_nav_tabs);
        }
        else {
          jQuery('.right-region').css('margin-top', '0');
        }
      });
      $('#block-views-special-events-carousel-block .slick-slider')
        .on('afterChange init', function(event, slick, direction){
          // Remove all prev/next.
          for (var i = 0; i < slick.$slides.length; i++) {
            console.log($(slick.$slides[i]));
            var $slide = $(slick.$slides[i]);
            if ($slide.hasClass('prev')) {
              $slide.removeClass('prev');
            };
            if ($slide.hasClass('next')) {
              $slide.removeClass('next');
            };
          }
          slick.$slides.removeClass('prev').removeClass('next');
          for (var i = 0; i < slick.$slides.length; i++) {
            var $slide = $(slick.$slides[i]);
            if ($slide.hasClass('slick-current')) {
              $slide.prev().addClass('prev');
              $slide.next().addClass('next');
              break;
            }
          }
        })
        .on('beforeChange', function(event, slick) {
          // Optional, but cleaner maybe.
          // Remove all prev/next.
          slick.$slides.removeClass('prev').removeClass('next');
        })
        $('#block-views-special-events-carousel-block .slick__slide').each(function(i) {
          if ($(this).hasClass('slick-active')) {
            $(this).prev().addClass('prev');
            $(this).next().addClass('next');
          };
        });
        $('#block-views-special-events-carousel-block .slick__slide').click(function() {
          if ($(this).hasClass('prev')) {
            $('#block-views-special-events-carousel-block .slick-prev').trigger('click');
          };
          if ($(this).hasClass('next')) {
            $('#block-views-special-events-carousel-block .slick-next').trigger('click');
          };
        });
    }
  };
  Drupal.behaviors.flexsliders = {
    attach: function(context, settings) {
      var images = jQuery(".view-id-promoted_slider").find(".img-responsive").length;
      if(images < 3) {
        window.setTimeout(function() {
          jQuery(".view-id-promoted_slider .flex-prev").toggle();
          jQuery(".view-id-promoted_slider .flex-next").toggle();
        }, 0);
      }
    }
  };
  Drupal.behaviors.displaysuitetabs = {
    attach: function(context, settings) {
      if (jQuery("#field-display-overview-wrapper").length) {
        jQuery("ul.vertical-tabs-list").children().each(function(i){
          jQuery("ul.vertical-tabs-list li").removeClass( "active selected" );
          jQuery("ul.vertical-tabs-list li.first").addClass( "active selected" );
          jQuery(".vertical-tabs-processed" ).each(function( index ) {
            jQuery( this ).find(".vertical-tabs-processed fieldset").removeClass("in active");
            jQuery( this ).find(".vertical-tabs-processed fieldset div").removeClass("in active");
          });
          jQuery(".vertical-tabs-processed fieldset:eq(0)").addClass("in active");
          jQuery(".vertical-tabs-processed fieldset:eq(0) div").addClass("in active");
          $(this).click(function(){
            jQuery( ".vertical-tabs-processed" ).each(function( index ) {
              jQuery( this ).find("fieldset").removeClass("in active");
              jQuery( this ).find("fieldset div").removeClass("in active");
            });
            var index = i;
            jQuery( ".vertical-tabs-processed" ).each(function( index ) {
              jQuery( this ).find("fieldset:eq(" + i + ")").addClass("in active");
              jQuery( this ).find("fieldset:eq(" + i + ") div").addClass("in active");
            });
          });
        });
      }
    }
  };
})(jQuery);
