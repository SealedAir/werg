<?php
/**
 * @file
 * Template overrides for seeportals theme.
 */

/**
 * Implements hook_form_alter().
 */
function seeportals_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    // Modify the User Login Block Form.
    case 'user_login_block':
      $item = array();
      $items[] = l(t('Forgot your password?'), 'user/password', array('html' => TRUE));
      $form['links']['#markup'] = theme('item_list', array('items' => $items));
      unset($form['name']['#title']);
      unset($form['pass']['#title']);
      break;

    case 'search_block_form':
      $form['search_block_form']['#attributes']['placeholder'] = t('Enter your search term...');
      break;

    case 'views_exposed_form':
      if (isset($form['dynamic-field'])) {
        $form['dynamic-field']['#prefix'] = '<div class="modal-footer"><button type="button" id="btn-close-modal" class="btn btn-default" data-dismiss="modal">Close</button></div>';
      }
      break;

  }
}

/**
 * Implements hook_image_style().
 */
function seeportals_preprocess_image_style(&$vars) {
  $vars['attributes']['class'][] = 'img-responsive';
}

/**
 * TODO: Name function.
 */
function seeportals_file_link($variables) {
  $file = $variables['file'];
  $icon_directory = $variables['icon_directory'];

  $url = file_create_url($file->uri);
  $icon = theme('file_icon', array('file' => $file, 'icon_directory' => $icon_directory));

  $options = array(
    'attributes' => array(
      'type' => $file->filemime . '; length=' . $file->filesize,
    ),
  );

  // Use the description as the link text if available.
  if (empty($file->description)) {
    $link_text = $file->filename;
  }
  else {
    $link_text = $file->description;
    $options['attributes']['title'] = check_plain($file->filename);
  }
  $size = $file->filesize / 1024;

  if ($variables['file']->type == 'image') {
    $icon_t = array(
      'path' => $variables['file']->uri,
      'style_name' => 'thumbnail',
      'alt' => $variables['file']->alt,
      'title' => $variables['file']->title,
    );
    $thumbnail = theme('image_style', $icon_t);
    return '<span class="file-upload">' . $thumbnail . ' ' . l($link_text, $url, $options) . ' (' . round($size, 2) . ' KB)' . '</span>';
  }
  else {
    $theme_path = drupal_get_path('theme', 'seeportals');
    $icon_d = array(
      'path' => $theme_path . '/images/document.png',
      'alt' => $variables['file']->alt,
      'title' => $variables['file']->title,
      'width' => '45px',
      'height' => '40px',
      'attributes' => array('class' => 'file-icon'),
    );
    $document = theme('image', $icon_d);
    return '<span class="file-upload">' . $document . ' ' . l($link_text, $url, $options) . ' (' . round($size, 2) . ' KB)' . '</span>';
  }
}

/**
 * Implements theme_preprocess_field().
 */
function seeportals_preprocess_field(&$variables) {
  if ($variables['element']['#field_name'] == "field_paragraph") {
    $fcs = array();
    foreach (element_children($variables['element']) as $key) {
      $fcs[] = array_pop($variables['element'][$key]['entity']['field_collection_item']);
    }
    foreach ($fcs as $fc) {
      if (isset($fc['field_paragraph_style']['#items'][0]['tid'])) {
        $class_obj = taxonomy_term_load($fc['field_paragraph_style']['#items'][0]['tid']);
        $class_name = $class_obj->field_class[LANGUAGE_NONE][0]['value'];
        $variables['classes_array'][] = $class_name;
      }
    }
  }
}

/**
 * Implements theme_preprocess_page().
 */
function seeportals_preprocess_page(&$variables) {
  global $language;
  global $user;

  if ((request_path() == 'admin/content/file') || (request_path() == 'admin/content/file/thumbnails')) {
    $variables['title'] = t('Media Gallery');
    if ((!user_has_role('3', $variables['user'])) || ($variables['user']->uid != 1)) {
      unset($variables['tabs']['#primary']);
    }
    if (((request_path() == "admin/content/file") || (request_path() == "admin/content/file/thumbnails")) && (isset($variables['page']['content']['system_main']['operation'])) && ($variables['page']['content']['system_main']['operation']['#value'] == "delete")) {
      unset($variables['action_links']);
      unset($variables['tabs']);
    }
  }
  if (request_path() == 'file/add') {

    if ((!user_has_role(user_role_load_by_name('administrator')->rid, $variables['user'])) || ($variables['user']->uid != 1)) {
      unset($variables['tabs']['#primary']);
    }
  }
  if (request_path() == 'admin/people/create') {
    $variables['title'] = t('Add new account');
  }
  $mobile = preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
  if ($mobile == 1) {
    unset($variables['page']['slider']['views_promoted_slider-block_2']);
  }
  if(drupal_is_front_page()) {
    $key = new stdClass();
    $welcomeblockid = '';
    foreach($variables['page']['content'] as $key => $value) {
      if (isset($value['#block']->css_class)) {
          if($value['#block']->css_class == 'welcome-message-block') {
            $welcomeblockid = $key;
          }
      }
    }
    if(!isset($variables['page']['content'][$welcomeblockid]) && isset($variables['page']['content']['views_testimonial_slider-block']) && isset($variables['page']['content']['views_partners-block'])){
      $variables['page']['content']['views_testimonial_slider-block']['#block']->css_class = $variables['page']['content']['views_testimonial_slider-block']['#block']->css_class . " arrangement1";
      $variables['page']['content']['views_partners-block']['#block']->css_class = $variables['page']['content']['views_partners-block']['#block']->css_class . " arrangement1";
    }
    if(!isset($variables['page']['content'][$welcomeblockid]) && !isset($variables['page']['content']['views_partners-block']) && isset($variables['page']['content']['views_testimonial_slider-block'])){
      $variables['page']['content']['views_testimonial_slider-block']['#block']->css_class = $variables['page']['content']['views_testimonial_slider-block']['#block']->css_class . " arrangement2";
    }
    if(!isset($variables['page']['content'][$welcomeblockid]) && !isset($variables['page']['content']['views_testimonial_slider-block']) && isset($variables['page']['content']['views_partners-block'])){
      $variables['page']['content']['views_partners-block']['#block']->css_class = $variables['page']['content']['views_partners-block']['#block']->css_class . " arrangement3";
    }
    if(isset($variables['page']['content'][$welcomeblockid]) && !isset($variables['page']['content']['views_testimonial_slider-block']) && !isset($variables['page']['content']['views_partners-block'])){
      $variables['page']['content'][$welcomeblockid]['#block']->css_class =$variables['page']['content'][$welcomeblockid]['#block']->css_class . " arrangement4";
    }
    if(isset($variables['page']['content'][$welcomeblockid]) && isset($variables['page']['content']['views_testimonial_slider-block']) && isset($variables['page']['content']['views_partners-block'])){
      $variables['page']['content'][$welcomeblockid]['#block']->css_class =$variables['page']['content'][$welcomeblockid]['#block']->css_class . " complete";
    }
  }
}

/**
 * Implements theme_preprocess_html().
 */
function seeportals_preprocess_html(&$variables) {
  if ((drupal_is_front_page()) && (!isset($variables['page']['slider']))) {
    $variables['classes_array'][] = 'no-slider';
  }
}

/**
 * Implements theme_preprocess_node().
 */
function seeportals_preprocess_node(&$vars) {
  unset($vars['content']['links']['statistics']['#links']['statistics_counter']['title']);
}
