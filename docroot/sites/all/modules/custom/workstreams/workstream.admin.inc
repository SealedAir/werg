<?php

/**
 *
 */
function workstream_admin_form($form, &$form_state) {
  $form['workstream_tree'] = array(
    '#type' => 'fieldset',
    '#title' => t('Workstream tree'),
  );

  $form['workstream'] = array(
    '#type' => 'fieldset',
    '#title' => t('Workstream'),
  );

  $form['workstream_tree']['workstream_tree_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Workstream tree name'),
    '#default_value' => variable_get('workstream_tree_name'),
    '#description' => t('Enter a workstream tree name.'),
  );

  $form['workstream_tree']['workstream_edit_tree_link'] = array(
    '#type' => 'link',
    '#title' => t('Edit Tree'),
    '#href' => 'admin/structure/taxonomy/workstream_tree',
  );

  $form['workstream']['workstream_name_singular'] = array(
    '#type' => 'textfield',
    '#title' => t('Workstream name singular'),
    '#default_value' => variable_get('workstream_name_singular'),
    '#description' => t('Enter a workstream singular name.'),
  );

  $form['workstream']['workstream_name_plural'] = array(
    '#type' => 'textfield',
    '#title' => t('Workstream name plural'),
    '#default_value' => variable_get('workstream_name_plural'),
    '#description' => t('Enter a workstream plural name.'),
  );

  $form['workstream']['workstream_add_content_link'] = array(
    '#type' => 'link',
    '#title' => t('Add Workstream'),
    '#href' => 'node/add/workstream',
  );

  //$form['#validate'][] = 'nasdaq_rewrite_domain_validate';
  $form['#submit'][] = 'workstream_tree_name_submit';

  return (system_settings_form($form));
}

function workstream_tree_name_submit($form, &$form_state) {
  if (!empty($form['workstream_tree']['workstream_tree_name']['#value'])) {
    $new_name = check_plain($form['workstream_tree']['workstream_tree_name']['#value']);
    $workstream_tree = taxonomy_vocabulary_machine_name_load('workstream_tree');
    if ($workstream_tree->name != $new_name) {
      $workstream_tree->name = $new_name;
      taxonomy_vocabulary_save($workstream_tree);
    }
  }
  else {
    $workstream_tree = taxonomy_vocabulary_machine_name_load('workstream_tree');
    $workstream_tree->name = t('Workstream tree');
    taxonomy_vocabulary_save($workstream_tree);
  }
}
