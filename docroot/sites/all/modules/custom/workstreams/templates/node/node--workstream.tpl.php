<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php if ($type != 'workstream'): ?>
    <?php print $user_picture; ?>

    <?php print render($title_prefix); ?>
    <?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>

    <?php if ($display_submitted): ?>
      <div class="submitted">
        <?php print $submitted; ?>
      </div>
    <?php endif; ?>

    <div class="content"<?php print $content_attributes; ?>>
      <?php
        // We hide the comments and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
        print render($content);
      ?>
    </div>

    <?php print render($content['links']); ?>

    <?php print render($content['comments']); ?>
  <?php endif; ?>
  <?php if ($type == 'workstream'): ?>
    <div class="index-custom-link">
    <?php if (isset($index_link)): ?>
      <?php print $index_link; ?>
    <?php endif;?>
    </div>
    <div class="user-picture">
    <?php if (isset($user_picture_workstream)): ?>
      <?php print $user_picture_workstream; ?>
    <?php endif;?>
    </div>
    <h2<?php print $title_attributes; ?>><a class="node-title" href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>

    <?php if (!empty($workflow_breadcrumb)): ?>
      <div class="workflow-states">
        <?php print $workflow_breadcrumb; ?>
      </div>
    <?php endif; ?>

    <?php if ($display_submitted): ?>
      <div class="submitted">
        <?php print $submitted; ?>
      </div>
    <?php endif; ?>

    <div class="content"<?php print $content_attributes; ?>>
      <?php
        // We hide the comments and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
        //print render($content);
      ?>
      <div class="workstream-left-content">
      <?php if (isset($list_images_main)): ?>
        <div id="slides" class="flexslider">
          <ul class="slides">
            <?php print $list_images_main; ?>
          </ul>
        </div>
        <div id="carousel-control" class="flexslider">
          <ul class="slides">
            <?php print $list_images_main; ?>
          </ul>
        </div>
      <?php endif; ?>
      <?php if (isset($content['field_files_workstream'])): ?>
        <?php print render($content['field_files_workstream']); ?>
      <?php endif; ?>
      <?php if (isset($content['field_use_workflow_workstream']['#items'][0]['value'])): ?>
        <?php if ($content['field_use_workflow_workstream']['#items'][0]['value'] == '1'): ?>
          <?php if (isset($content['field_workflow_workstream'][0]['#markup'])): ?>
            <div class="state-<?php print $content['field_workflow_workstream']['#items'][0]['value'];?>">
              <?php print t('Status ') ?>
              <span class="state">
                <?php print render($content['field_workflow_workstream'][0]['#markup']); ?>
              </span>
            </div>
          <?php endif; ?>
        <?php endif; ?>
      <?php endif; ?>

      <?php if (isset($content['field_rating_name_1_workstrem']['#items'][0]['value'])): ?>
        <div class="rate-design">
          <?php print render($content['field_rating_name_1_workstrem']['#items'][0]['value']); ?>
          <span><?php print $average_r1; ?></span>
        </div>
      <?php endif; ?>

      <?php if (isset($content['field_rating_name_2_workstream']['#items'][0]['value'])): ?>
        <div class="rate-package">
          <?php print render($content['field_rating_name_2_workstream']['#items'][0]['value']); ?>
          <span><?php print $average_r2; ?></span>
        </div>
      <?php endif; ?>

      <div class="comments">
        <?php print t('Comments'); ?>
        <span><?php print $comment_count; ?></span>
      </div>

      <?php if (isset($content['field_description_workstream']['#items'][0]['value'])): ?>
        <div class="content">
          <div class="description">
            <?php print render($content['field_description_workstream']['#items'][0]['value']); ?>
          </div>
        </div>
      <?php endif; ?>
      </div>
      <div class="workstream-right-content">
      <?php if (isset($list_images)): ?>
        <div id="carousel" class="flexslider">
          <ul class="slides">
            <?php print $list_images; ?>
          </ul>
        </div>
      <?php endif; ?>
        <div class="details">
        <?php if (isset($content['field_rating_name_1_workstrem']['#items'][0]['value'])): ?>
           <div class="rate-design">
            <?php print render($content['field_rating_name_1_workstrem']['#items'][0]['value']); ?>
            <div><?php print render($content['field_workstream_r1']); ?></div>
            <div class="votes"><?php print t('@count votes', array('@count' => $content['field_workstream_r1']['#items'][0]['count'])); ?></div>
          </div>
        <?php endif; ?>

        <?php if (isset($content['field_rating_name_2_workstream']['#items'][0]['value'])): ?>
          <div class="rate-package">
            <?php print render($content['field_rating_name_2_workstream']['#items'][0]['value']); ?>
            <div><?php print render($content['field_workstream_r2']); ?></div>
            <div class="votes"><?php print t('@count votes', array('@count' => $content['field_workstream_r2']['#items'][0]['count'])); ?></div>
          </div>
        <?php endif; ?>

        <?php if (isset($content['field_color_palette_workstream'])): ?>
          <div class="color-pallete">
            <?php print t('Color palette');?>
            <?php print render($content['field_color_palette_workstream']); ?>
          </div>
        <?php endif; ?>

        <?php if (isset($content['field_image_downloads_workstream'])): ?>
          <div class="download-sizes">
            <div class="label"><?php print t('Download');?></div>
            <div class="content">
               <?php print render($image_downloads_workstream); ?>
              <a class="zip-image" href="/cargill/workstream-download-image/<?php print $node->nid; ?>"><?php print t('Download images');?></a>
            </div>

          </div>
        <?php endif; ?>

        <?php if (isset($content['field_attached_file_workstream'])): ?>
          <div class="attached-file">
            <div class="label"><?php print t('Attached file');?></div>
            <div class="block-content">
             <?php print render($attached_file_workstream); ?>
             <a class="zip-files" href="/cargill/workstream-download-file/<?php print $node->nid; ?>"><?php print t('Download files');?></a>
            </div>
          </div>
        <?php endif; ?>

      <?php if (isset($workflow_status)): ?>
          <div class="status-average">
            <div class="label"><?php print t('Status');?></div>
            <div class="block-content"><?php print render($workflow_status); ?></div>
          </div>
        <?php endif; ?>
        </div>
      </div>

    </div>

    <?php print render($content['comments']); ?>
  <?php endif; ?>

</div>
