<?php
/**
 * @file
 * workstreams_views_workstream_tree.features.inc
 */

/**
 * Implements hook_views_api().
 */
function workstreams_views_workstream_tree_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
