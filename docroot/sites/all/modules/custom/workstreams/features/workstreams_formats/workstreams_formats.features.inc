<?php
/**
 * @file
 * workstreams_formats.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function workstreams_formats_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_fe_date_custom_date_formats().
 */
function workstreams_formats_fe_date_custom_date_formats() {
  $custom_date_formats = array();
  $custom_date_formats['M d, Y'] = 'M d, Y';
  return $custom_date_formats;
}

/**
 * Implements hook_date_format_types().
 */
function workstreams_formats_date_format_types() {
  $format_types = array();
  // Exported date format type: workstream
  $format_types['workstream'] = 'Workstream';
  return $format_types;
}

/**
 * Implements hook_image_default_styles().
 */
function workstreams_formats_image_default_styles() {
  $styles = array();

  // Exported image style: workstream_last_child.
  $styles['workstream_last_child'] = array(
    'label' => 'Workstream last child',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 250,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: workstream_project.
  $styles['workstream_project'] = array(
    'label' => 'Workstream project',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 55,
          'height' => 55,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: workstream_project_image.
  $styles['workstream_project_image'] = array(
    'label' => 'Workstream project image',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 600,
          'height' => 390,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: workstream_thumbnail.
  $styles['workstream_thumbnail'] = array(
    'label' => 'Workstream Thumbnail',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 175,
          'height' => 150,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
