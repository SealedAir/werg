<?php
/**
 * @file
 * workstreams_roles.captcha.inc
 */

/**
 * Implements hook_captcha_default_points().
 */
function workstreams_roles_captcha_default_points() {
  $export = array();

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_workstream_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_workstream_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'user_login';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['user_login'] = $captcha;

  return $export;
}
