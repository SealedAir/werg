<?php
/**
 * @file
 * workstreams_roles.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function workstreams_roles_user_default_roles() {
  $roles = array();

  // Exported role: Workstream admin.
  $roles['Workstream admin'] = array(
    'name' => 'Workstream admin',
    'weight' => 3,
    'uuid' => 'daa30568-3706-47fa-bdfd-15b82591af64',
  );

  // Exported role: Workstream customer.
  $roles['Workstream customer'] = array(
    'name' => 'Workstream customer',
    'weight' => 5,
    'uuid' => '3ef2caca-72fb-48ac-b28f-2098bda3cce1',
  );

  // Exported role: Workstream editor.
  $roles['Workstream editor'] = array(
    'name' => 'Workstream editor',
    'weight' => 4,
    'uuid' => '4c9ac8f2-388f-406d-8906-d4d6abf4b9d8',
  );

  return $roles;
}
