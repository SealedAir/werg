<?php
/**
 * @file
 * workstreams_taxonomy_workstream_tags.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function workstreams_taxonomy_workstream_tags_taxonomy_default_vocabularies() {
  return array(
    'workstream_tags' => array(
      'name' => 'Workstream Tags',
      'machine_name' => 'workstream_tags',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
