<?php
/**
 * @file
 * workstreams_ct_workstream.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function workstreams_ct_workstream_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_features_actions_triggers_export_actions_default().
 */
function workstreams_ct_workstream_features_actions_triggers_export_actions_default() {
  $items = array(
    'workstream_log:workstream_log' => array(
      'aid' => 'workstream_log',
      'type' => 'comment',
      'callback' => 'workstream_log',
      'parameters' => '',
      'label' => 'Workstream log',
    ),
  );
  return $items;
}

/**
 * Implements hook_features_actions_triggers_export_triggers_default().
 */
function workstreams_ct_workstream_features_actions_triggers_export_triggers_default() {
  $items = array(
    'comment_presave:workstream_log:workstream_log' => array(
      'hook' => 'comment_presave',
      'aid' => 'workstream_log',
      'weight' => 1,
    ),
  );
  return $items;
}

/**
 * Implements hook_node_info().
 */
function workstreams_ct_workstream_node_info() {
  $items = array(
    'workstream' => array(
      'name' => t('Workstream'),
      'base' => 'node_content',
      'description' => 'By using this content, customers and editors can work together,
	   offering feedback on new projects, designs and documents. This content will offer 
	   the neccesary feedback from the persons included in the projects, to the workstream creators.',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
