<?php
/**
 * @file
 * workstreams_views_comments_block.features.inc
 */

/**
 * Implements hook_views_api().
 */
function workstreams_views_comments_block_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
