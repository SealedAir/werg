<?php
/**
 * @file
 * workstreams_views_workstream_tree_projects.features.inc
 */

/**
 * Implements hook_views_api().
 */
function workstreams_views_workstream_tree_projects_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
