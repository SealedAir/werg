(function($) {
  /*Workstream slider*/
  Drupal.behaviors.workstreamSlider = {
    attach: function (context, settings) {
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        direction: "vertical"
      });

       $('#carousel-control').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 150,
        minItems: 3,
        maxItems: 3,
        direction: "horizontal",
        asNavFor: '#slides'
      });

      $('#slides').flexslider({
        animation: "fade",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
         sync: "#carousel-control",
      });

      $(window).resize(function(){
        var height_slider = $('#slides').height();
        $('.three-img').css('height', height_slider);
        $('#carousel .flex-viewport').css('height', height_slider);
        if(749 < $(this).width() &&  $(this).width() <= 882) {
          $('.workstream-right-content .details').css('margin-top', height_slider + 85);
        }
        else {
          $('.workstream-right-content .details').css('margin-top', 0);
        }
          var img_height = $("#carousel img").height();
          var margin = (height_slider - 3*img_height)/2;
          $("#carousel img").css('margin-bottom', margin);
        })
        var height_slider = $('#slides').height();
        $('#carousel .flex-viewport').css('height', height_slider);

         if(749 < $(window).width() &&  $(window).width() <= 882) {
          $('.workstream-right-content .details').css('margin-top', height_slider + 85);
        }
        else {
          $('.workstream-right-content .details').css('margin-top', 0);
        }
        window.onload = function() {
          var height_slider = $('#slides').height();
          $('#carousel .flex-viewport').css('height', height_slider);
          $('.three-img').css('height', height_slider);
          var img_height = $("#carousel img").height();
          var margin = (height_slider - 3*img_height)/2;
          $("#carousel img").css('margin-bottom', margin);
        }
      //if ($('.workstream-right-content .flex-active-slide').children().length> 1) {
        $('#carousel li img').click(function() {
          var id = $(this).attr('id');
            $('#slides li').removeClass('flex-active-slide');
            $('#slides li').css({
              'opacity': 0,
              'z-index': 1,
              'transition-duration': '0.6s'
            })
          $('#slides li img').each(function(){
            if($(this).attr('data-fid') == id) {
              $(this).parent().addClass('flex-active-slide');
              $(this).parent().css({
                'opacity': 1,
                'z-index': 2,
                'transition-duration': '0.6s'
              })
            }
          })
        })
      //}
    }
  }

})(jQuery);