<?php
/**
 * @file
 * Block template for the map.
 */
?>
<div id="map" style="height:400px;"></div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBHop3nEommg9a8S9iDXFTHe4NyAxRfEPQ&sensor=false"></script>
<script type="text/javascript">
  var latLng = new google.maps.LatLng(<?php print $lat . ',' . $longi; ?>);
  var mapOptions = {
    zoom: 11,
    center: latLng,
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles:[
    {
      "featureType": "administrative",
      "elementType": "labels.text.fill",
      "stylers": [
      {
        "color": "#444444"
      }
      ]
    },
    {
      "featureType": "landscape",
      "elementType": "all",
      "stylers": [
      {
        "color": "#f2f2f2"
      }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "all",
      "stylers": [
      {
        "visibility": "off"
      }
      ]
    },
    {
      "featureType": "road",
      "elementType": "all",
      "stylers": [
      {
        "saturation": -100
      },
      {
        "lightness": 45
      }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "all",
      "stylers": [
      {
        "visibility": "simplified"
      }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "labels.icon",
      "stylers": [
      {
        "visibility": "off"
      }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "all",
      "stylers": [
      {
        "visibility": "off"
      }
      ]
    },
    {
      "featureType": "water",
      "elementType": "all",
      "stylers": [
      {
        "color": "#008c99"
      },
      {
        "visibility": "on"
      }
      ]
    }
    ]
  };

  var map = new google.maps.Map(document.getElementById('map'), mapOptions);
  var flagIcon_front = new google.maps.MarkerImage("<?php  print base_path() . drupal_get_path('module', 'partner_portal_block_custom_map'); ?>/images/icon.png");
  var url='https://www.google.ro/maps/place/<?php print $lat . ',' . $longi; ?>';
  var marker = new google.maps.Marker({
    position: latLng,
    map: map,
    icon:flagIcon_front,
    draggable: false,
    url: url
  });
  google.maps.event.addListener(marker, 'click', function() {
    window.location.href = this.url;
  });
</script>
