<?php
/**
 * @file
 * Template file.
 */
?>
<dt>
  <?php print l(t("Manage templates of entity Template A"), 'admin/structure/entity-type/template_a/template_a'); ?>
</dt>
<dd>
  <?php print t('Add, edit or delete templates A'); ?>
</dd>
<dt>
  <?php print l(t("Manage templates of entity Template B"), 'admin/structure/entity-type/template_b/template_b'); ?>
</dt>
<dd>
  <?php print t('Add, edit or delete templates B'); ?>
</dd>
