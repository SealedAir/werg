<?php
/**
 * @file
 * pp_blocks_events_carousel.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function pp_blocks_events_carousel_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-special_events_carousel-block'] = array(
    'cache' => -1,
    'css_class' => 'events-carousel-block',
    'custom' => 0,
    'delta' => 'special_events_carousel-block',
    'i18n_block_language' => array(),
    'i18n_mode' => 0,
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
      'seeportals' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'seeportals',
        'weight' => -17,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
