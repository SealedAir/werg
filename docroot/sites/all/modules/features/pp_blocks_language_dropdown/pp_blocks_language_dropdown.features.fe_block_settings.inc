<?php
/**
 * @file
 * pp_blocks_language_dropdown.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function pp_blocks_language_dropdown_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['lang_dropdown-language'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'language',
    'i18n_block_language' => array(),
    'i18n_mode' => 0,
    'module' => 'lang_dropdown',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'seeportals' => array(
        'region' => 'header_top',
        'status' => 1,
        'theme' => 'seeportals',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
