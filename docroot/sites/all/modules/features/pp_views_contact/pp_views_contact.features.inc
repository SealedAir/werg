<?php
/**
 * @file
 * pp_views_contact.features.inc
 */

/**
 * Implements hook_views_api().
 */
function pp_views_contact_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
