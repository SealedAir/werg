<?php
/**
 * @file
 * pp_blocks_partners.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function pp_blocks_partners_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-partners-block'] = array(
    'cache' => -1,
    'css_class' => 'partners-list-block',
    'custom' => 0,
    'delta' => 'partners-block',
    'i18n_block_language' => array(),
    'i18n_mode' => 0,
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
      'seeportals' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'seeportals',
        'weight' => -20,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
