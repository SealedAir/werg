<?php
/**
 * @file
 * pp_breakpoints_desktop.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_breakpoints_desktop_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "breakpoints" && $api == "default_breakpoints") {
    return array("version" => "1");
  }
}
