<?php
/**
 * @file
 * pp_content_type_events.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_content_type_events_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function pp_content_type_events_node_info() {
  $items = array(
    'event' => array(
      'name' => t('Event'),
      'base' => 'node_content',
      'description' => t('Add event content through which you can promote events or 
      other activities on both the front page slider and the dedicated events 
      list page.Photos or attachments can be added along the description.
      The content can point to both the created content or an external link 
      (e.g. source of the content).'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
