<?php
/**
 * @file
 * pp_users_role_super_editor.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function pp_users_role_super_editor_user_default_roles() {
  $roles = array();

  // Exported role: Super-editor.
  $roles['Super-editor'] = array(
    'name' => 'Super-editor',
    'weight' => 4,
  );

  return $roles;
}
