<?php
/**
 * @file
 * pp_menus_main_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function pp_menus_main_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_contact-us:<void2>.
  $menu_links['main-menu_contact-us:<void2>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<void2>',
    'router_path' => '<void2>',
    'link_title' => 'Contact us',
    'options' => array(
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'page-scroll',
        'style' => '',
      ),
      'domain_menu_access' => array(
        'hide' => array(
          'd66' => 'd66',
        ),
      ),
      'identifier' => 'main-menu_contact-us:<void2>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_home:<front>.
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_news:news.
  $menu_links['main-menu_news:news'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'news',
    'router_path' => 'news',
    'link_title' => 'News',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'main-menu_news:news',
      'attributes' => array(
        'title' => 'NEWS',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Contact us');
  t('Home');
  t('News');

  return $menu_links;
}
