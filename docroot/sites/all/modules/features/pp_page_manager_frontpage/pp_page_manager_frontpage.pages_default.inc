<?php
/**
 * @file
 * pp_page_manager_frontpage.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function pp_page_manager_frontpage_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'frontpage';
  $page->task = 'page';
  $page->admin_title = 'Frontpage';
  $page->admin_description = '';
  $page->path = 'home';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_frontpage__panel_context_b3472ea7-1068-4008-a22a-7593c523026f';
  $handler->task = 'page';
  $handler->subtask = 'frontpage';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Homepage',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 1,
          1 => 2,
          2 => 3,
        ),
        'parent' => 'canvas',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'top',
        ),
        'parent' => 'main',
        'class' => 'front-main-container',
        'hide_empty' => 0,
      ),
      2 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'welcome',
          1 => 'featured_video',
        ),
        'parent' => 'main',
        'class' => 'front-main-container panel-welcome',
        'hide_empty' => 0,
      ),
      'welcome' => array(
        'type' => 'region',
        'title' => 'Welcome',
        'width' => '59.970628369752085',
        'width_type' => '%',
        'parent' => '2',
        'class' => 'col-lg-8 col-md-8 col-sm-12 col-xs-12 video-block ',
        'hide_empty' => 0,
      ),
      'featured_video' => array(
        'type' => 'region',
        'title' => 'Featured video',
        'width' => '40.029371630247915',
        'width_type' => '%',
        'parent' => '2',
        'class' => 'col-lg-4 col-md-4 col-sm-12 col-xs-12 quick-links',
        'hide_empty' => 0,
      ),
      3 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center_',
        ),
        'parent' => 'main',
        'class' => 'front-main-container',
        'hide_empty' => 0,
      ),
      'center_' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => 100,
        'width_type' => '%',
        'parent' => '3',
        'class' => '',
        'hide_empty' => 0,
      ),
      'top' => array(
        'type' => 'region',
        'title' => 'Top',
        'width' => 100,
        'width_type' => '%',
        'parent' => '1',
        'class' => '',
        'hide_empty' => 0,
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'promoted_slider' => NULL,
      'welcome' => NULL,
      'featured_video' => NULL,
      'center_' => NULL,
      'contact_form' => NULL,
      'contact' => NULL,
      'map' => NULL,
      'slider_carousel' => NULL,
      'top' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '<none>';
  $display->uuid = '6dac0fdb-a290-491c-b4b0-17f099457777';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-1099acce-a2a6-4f07-adac-5f8a5f1637a8';
    $pane->panel = 'center_';
    $pane->type = 'views';
    $pane->subtype = 'special_events';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'domain',
          'settings' => array(
            'domain_site' => 0,
            'domains' => array(
              'partners_dev_sealedair_com' => 'partners_dev_sealedair_com',
              'italy_partners_dev_sealedair_com' => 'italy_partners_dev_sealedair_com',
              'belgium_partners_dev_sealedair_com' => 'belgium_partners_dev_sealedair_com',
              'poland_partners_dev_sealedair_com' => 'poland_partners_dev_sealedair_com',
              'nl_partners_dev_sealedair_com' => 'nl_partners_dev_sealedair_com',
              'global_partners_dev_sealedair_com' => 0,
            ),
          ),
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'events',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1099acce-a2a6-4f07-adac-5f8a5f1637a8';
    $display->content['new-1099acce-a2a6-4f07-adac-5f8a5f1637a8'] = $pane;
    $display->panels['center_'][0] = 'new-1099acce-a2a6-4f07-adac-5f8a5f1637a8';
    $pane = new stdClass();
    $pane->pid = 'new-7f7ec3cc-a563-4d79-a77a-ad78a15f5645';
    $pane->panel = 'featured_video';
    $pane->type = 'views_panes';
    $pane->subtype = 'homepage_welcome-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'domain',
          'settings' => array(
            'domain_site' => 0,
            'domains' => array(
              'partners_dev_sealedair_com' => 'partners_dev_sealedair_com',
              'italy_partners_dev_sealedair_com' => 'italy_partners_dev_sealedair_com',
              'belgium_partners_dev_sealedair_com' => 'belgium_partners_dev_sealedair_com',
              'poland_partners_dev_sealedair_com' => 'poland_partners_dev_sealedair_com',
              'nl_partners_dev_sealedair_com' => 'nl_partners_dev_sealedair_com',
              'global_partners_dev_sealedair_com' => 0,
            ),
          ),
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7f7ec3cc-a563-4d79-a77a-ad78a15f5645';
    $display->content['new-7f7ec3cc-a563-4d79-a77a-ad78a15f5645'] = $pane;
    $display->panels['featured_video'][0] = 'new-7f7ec3cc-a563-4d79-a77a-ad78a15f5645';
    $pane = new stdClass();
    $pane->pid = 'new-61a1a531-78d6-44d9-9390-fb496d63e4e3';
    $pane->panel = 'top';
    $pane->type = 'views';
    $pane->subtype = 'news';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'domain',
          'settings' => array(
            'domain_site' => 0,
            'domains' => array(
              'partners_dev_sealedair_com' => 'partners_dev_sealedair_com',
              'italy_partners_dev_sealedair_com' => 'italy_partners_dev_sealedair_com',
              'belgium_partners_dev_sealedair_com' => 'belgium_partners_dev_sealedair_com',
              'poland_partners_dev_sealedair_com' => 'poland_partners_dev_sealedair_com',
              'nl_partners_dev_sealedair_com' => 'nl_partners_dev_sealedair_com',
              'global_partners_dev_sealedair_com' => 0,
            ),
          ),
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '3',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'news',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '61a1a531-78d6-44d9-9390-fb496d63e4e3';
    $display->content['new-61a1a531-78d6-44d9-9390-fb496d63e4e3'] = $pane;
    $display->panels['top'][0] = 'new-61a1a531-78d6-44d9-9390-fb496d63e4e3';
    $pane = new stdClass();
    $pane->pid = 'new-c78374fc-eda3-42c9-8fb1-991584025133';
    $pane->panel = 'welcome';
    $pane->type = 'views_panes';
    $pane->subtype = 'homepage_welcome-panel_pane_2';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'domain',
          'settings' => array(
            'domain_site' => 0,
            'domains' => array(
              'partners_dev_sealedair_com' => 'partners_dev_sealedair_com',
              'italy_partners_dev_sealedair_com' => 'italy_partners_dev_sealedair_com',
              'belgium_partners_dev_sealedair_com' => 'belgium_partners_dev_sealedair_com',
              'poland_partners_dev_sealedair_com' => 'poland_partners_dev_sealedair_com',
              'nl_partners_dev_sealedair_com' => 'nl_partners_dev_sealedair_com',
              'global_partners_dev_sealedair_com' => 0,
            ),
          ),
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c78374fc-eda3-42c9-8fb1-991584025133';
    $display->content['new-c78374fc-eda3-42c9-8fb1-991584025133'] = $pane;
    $display->panels['welcome'][0] = 'new-c78374fc-eda3-42c9-8fb1-991584025133';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-1099acce-a2a6-4f07-adac-5f8a5f1637a8';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['frontpage'] = $page;

  return $pages;

}
