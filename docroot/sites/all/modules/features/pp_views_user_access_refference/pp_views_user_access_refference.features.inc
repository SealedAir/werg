<?php
/**
 * @file
 * pp_views_user_access_refference.features.inc
 */

/**
 * Implements hook_views_api().
 */
function pp_views_user_access_refference_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
