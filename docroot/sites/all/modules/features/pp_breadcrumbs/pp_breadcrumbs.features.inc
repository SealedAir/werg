<?php
/**
 * @file
 * pp_breadcrumbs.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_breadcrumbs_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function pp_breadcrumbs_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_custom_breadcrumb_features_default_settings().
 */
function pp_breadcrumbs_custom_breadcrumb_features_default_settings() {
  $custom_breadcrumbs = array();

  // Exported custom breadcrumb: basic_page_breadcrumb.
  $custom_breadcrumbs['basic_page_breadcrumb'] = array(
    'name' => 'Basic page breadcrumb',
    'titles' => '[node:title]',
    'paths' => '<none>',
    'visibility_php' => '',
    'node_type' => 'dvc_page',
    'language' => '',
    'machine_name' => 'basic_page_breadcrumb',
  );

  // Exported custom breadcrumb: events_node.
  $custom_breadcrumbs['events_node'] = array(
    'name' => 'Events node',
    'titles' => 'Events
[node:title]',
    'paths' => 'events
<none>',
    'visibility_php' => '',
    'node_type' => 'event',
    'language' => '',
    'machine_name' => 'events_node',
  );

  // Exported custom breadcrumb: news_node.
  $custom_breadcrumbs['news_node'] = array(
    'name' => 'News node',
    'titles' => 'News
[node:title]',
    'paths' => 'news
<none>',
    'visibility_php' => '',
    'node_type' => 'news',
    'language' => '',
    'machine_name' => 'news_node',
  );

  // Exported custom breadcrumb: template_a_node_type.
  $custom_breadcrumbs['template_a_node_type'] = array(
    'name' => 'Template A node type',
    'titles' => 'Partners Area
[node:title]',
    'paths' => '<none>
<none>',
    'visibility_php' => '',
    'node_type' => 'template_a',
    'language' => '',
    'machine_name' => 'template_a_node_type',
  );

  // Exported custom breadcrumb: template_b_node_type.
  $custom_breadcrumbs['template_b_node_type'] = array(
    'name' => 'Template B node type',
    'titles' => 'Partners Area
[node:title]',
    'paths' => '<none>
<none>',
    'visibility_php' => '',
    'node_type' => 'template_b',
    'language' => '',
    'machine_name' => 'template_b_node_type',
  );

  return $custom_breadcrumbs;
}

/**
 * Implements hook_custom_breadcrumbs_panels_features_default_settings().
 */
function pp_breadcrumbs_custom_breadcrumbs_panels_features_default_settings() {
  $custom_breadcrumbs = array();

  // Exported custom breadcrumb: promotions_list.
  $custom_breadcrumbs['promotions_list'] = array(
    'name' => 'Promotions list',
    'titles' => 'Promotions',
    'paths' => '<none>',
    'visibility_php' => '',
    'panel_id' => 'node_view__promotions',
    'language' => '',
    'machine_name' => 'promotions_list',
  );

  // Exported custom breadcrumb: promotions_node.
  $custom_breadcrumbs['promotions_node'] = array(
    'name' => 'Promotions node',
    'titles' => 'Promotions
[node:title]',
    'paths' => 'promotions
<none>',
    'visibility_php' => '',
    'panel_id' => 'node_view__promotions',
    'language' => '',
    'machine_name' => 'promotions_node',
  );

  return $custom_breadcrumbs;
}

/**
 * Implements hook_custom_breadcrumbs_paths_features_default_settings().
 */
function pp_breadcrumbs_custom_breadcrumbs_paths_features_default_settings() {
  $custom_breadcrumbs = array();

  // Exported custom breadcrumb: add_user_form.
  $custom_breadcrumbs['add_user_form'] = array(
    'name' => 'Add user form',
    'titles' => 'Add new account',
    'paths' => '<none>',
    'visibility_php' => '',
    'specific_path' => 'admin/people/create',
    'language' => '',
    'machine_name' => 'add_user_form',
  );

  // Exported custom breadcrumb: content.
  $custom_breadcrumbs['content'] = array(
    'name' => 'Content',
    'titles' => 'Content',
    'paths' => '',
    'visibility_php' => '',
    'specific_path' => 'admin/content',
    'language' => '',
    'machine_name' => 'content',
  );

  // Exported custom breadcrumb: file.
  $custom_breadcrumbs['file'] = array(
    'name' => 'File',
    'titles' => 'Media Gallery
[current-page:title]',
    'paths' => 'admin/content/file
<none>',
    'visibility_php' => '',
    'specific_path' => 'file/*',
    'language' => '',
    'machine_name' => 'file',
  );

  // Exported custom breadcrumb: file_1.
  $custom_breadcrumbs['file_1'] = array(
    'name' => 'File',
    'titles' => 'test',
    'paths' => '<none>',
    'visibility_php' => 'if (arg(2)) {
return FALSE;
}',
    'specific_path' => 'file/*',
    'language' => '',
    'machine_name' => 'file_1',
  );

  // Exported custom breadcrumb: manage_content.
  $custom_breadcrumbs['manage_content'] = array(
    'name' => 'Manage Content',
    'titles' => 'Manage content',
    'paths' => '</none>',
    'visibility_php' => '',
    'specific_path' => 'admin/manage/content',
    'language' => '',
    'machine_name' => 'manage_content',
  );

  // Exported custom breadcrumb: media_add_upload.
  $custom_breadcrumbs['media_add_upload'] = array(
    'name' => 'Media Add Upload',
    'titles' => 'Media Gallery
Add file',
    'paths' => 'admin/content/file
<none>',
    'visibility_php' => '',
    'specific_path' => 'file/add',
    'language' => '',
    'machine_name' => 'media_add_upload',
  );

  // Exported custom breadcrumb: media_add_upload_archive.
  $custom_breadcrumbs['media_add_upload_archive'] = array(
    'name' => 'Media Add Upload Archive',
    'titles' => 'Partners Area
Media Gallery
Add file',
    'paths' => '<none>
admin/content/file
<none>',
    'visibility_php' => '',
    'specific_path' => 'file/add/upload/archive',
    'language' => '',
    'machine_name' => 'media_add_upload_archive',
  );

  // Exported custom breadcrumb: media_add_web.
  $custom_breadcrumbs['media_add_web'] = array(
    'name' => 'Media Add Web',
    'titles' => 'Media Gallery
Add file',
    'paths' => 'admin/content/file
<none>',
    'visibility_php' => '',
    'specific_path' => 'file/add/web',
    'language' => '',
    'machine_name' => 'media_add_web',
  );

  // Exported custom breadcrumb: media_edit.
  $custom_breadcrumbs['media_edit'] = array(
    'name' => 'Media Edit',
    'titles' => 'Media Gallery
Edit',
    'paths' => 'admin/content/file
<none>',
    'visibility_php' => '',
    'specific_path' => 'file/*/edit',
    'language' => '',
    'machine_name' => 'media_edit',
  );

  // Exported custom breadcrumb: media_gallery_list.
  $custom_breadcrumbs['media_gallery_list'] = array(
    'name' => 'Media Gallery List',
    'titles' => 'Media Gallery',
    'paths' => '<none>',
    'visibility_php' => '',
    'specific_path' => 'admin/content/file',
    'language' => '',
    'machine_name' => 'media_gallery_list',
  );

  // Exported custom breadcrumb: media_gallery_thumbnails.
  $custom_breadcrumbs['media_gallery_thumbnails'] = array(
    'name' => 'Media Gallery Thumbnails',
    'titles' => 'Media Gallery',
    'paths' => '<none>',
    'visibility_php' => '',
    'specific_path' => 'admin/content/file/thumbnails',
    'language' => '',
    'machine_name' => 'media_gallery_thumbnails',
  );

  // Exported custom breadcrumb: node_add.
  $custom_breadcrumbs['node_add'] = array(
    'name' => 'Node Add',
    'titles' => 'Add content',
    'paths' => 'node/add',
    'visibility_php' => '',
    'specific_path' => 'node/add',
    'language' => '',
    'machine_name' => 'node_add',
  );

  // Exported custom breadcrumb: promotion_statistics.
  $custom_breadcrumbs['promotion_statistics'] = array(
    'name' => 'Promotion Statistics',
    'titles' => '[current-page:page-title]',
    'paths' => '<none>',
    'visibility_php' => '',
    'specific_path' => 'admin/promotion-stats/*',
    'language' => '',
    'machine_name' => 'promotion_statistics',
  );

  // Exported custom breadcrumb: user_profile.
  $custom_breadcrumbs['user_profile'] = array(
    'name' => 'User profile',
    'titles' => 'Partners Area
My account',
    'paths' => '<none>
<none>',
    'visibility_php' => '',
    'specific_path' => 'user',
    'language' => '',
    'machine_name' => 'user_profile',
  );

  // Exported custom breadcrumb: user_profile_1.
  $custom_breadcrumbs['user_profile_1'] = array(
    'name' => 'User profile',
    'titles' => 'Partners Area
My account',
    'paths' => '<none>
<none>',
    'visibility_php' => '',
    'specific_path' => 'user',
    'language' => '',
    'machine_name' => 'user_profile_1',
  );

  return $custom_breadcrumbs;
}

/**
 * Implements hook_custom_breadcrumbs_views_features_default_settings().
 */
function pp_breadcrumbs_custom_breadcrumbs_views_features_default_settings() {
  $custom_breadcrumbs = array();

  // Exported custom breadcrumb: events_list.
  $custom_breadcrumbs['events_list'] = array(
    'name' => 'Events list',
    'titles' => 'Events',
    'paths' => '<none>',
    'visibility_php' => '',
    'views_path' => 'events',
    'language' => '',
    'machine_name' => 'events_list',
  );

  // Exported custom breadcrumb: news_list.
  $custom_breadcrumbs['news_list'] = array(
    'name' => 'News list',
    'titles' => 'News',
    'paths' => '<none>',
    'visibility_php' => '',
    'views_path' => 'news',
    'language' => '',
    'machine_name' => 'news_list',
  );

  return $custom_breadcrumbs;
}
