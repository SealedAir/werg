<?php
/**
 * @file
 * pp_blocks_workbench_information.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function pp_blocks_workbench_information_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['workbench-block'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'block',
    'i18n_block_language' => array(),
    'i18n_mode' => 0,
    'module' => 'workbench',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'adminimal',
        'weight' => -39,
      ),
      'bootstrap' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'bootstrap',
        'weight' => -99,
      ),
      'seeportals' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seeportals',
        'weight' => 17,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
