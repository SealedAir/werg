<?php
/**
 * @file
 * pp_content_type_testimonial.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function pp_content_type_testimonial_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-testimonial-field_testimonial'.
  $field_instances['node-testimonial-field_testimonial'] = array(
    'bundle' => 'testimonial',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_testimonial',
    'label' => 'Testimonial',
    'required' => 1,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 41,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Testimonial');

  return $field_instances;
}
