<?php
/**
 * @file
 * pp_content_type_testimonial.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_content_type_testimonial_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function pp_content_type_testimonial_node_info() {
  $items = array(
    'testimonial' => array(
      'name' => t('Testimonial'),
      'base' => 'node_content',
      'description' => t('Add a testimonial that will be promoted through the front page testimonial slider and the testimonials page. This content can be used to promote testimonials or customer reviews.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
