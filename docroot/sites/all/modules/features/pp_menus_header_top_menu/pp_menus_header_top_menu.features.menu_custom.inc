<?php
/**
 * @file
 * pp_menus_header_top_menu.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function pp_menus_header_top_menu_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-header-top-menu.
  $menus['menu-header-top-menu'] = array(
    'menu_name' => 'menu-header-top-menu',
    'title' => 'Header top menu',
    'description' => 'The Header top menu contains links displayed on the top right corner. Used for quick access links, like "Search" or "Media Gallery".',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Header top menu');
  t('The Header top menu contains links displayed on the top right corner. Used for quick access links, like "Search" or "Media Gallery".');

  return $menus;
}
