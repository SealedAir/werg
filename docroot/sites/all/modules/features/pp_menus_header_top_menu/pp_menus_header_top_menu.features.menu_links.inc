<?php
/**
 * @file
 * pp_menus_header_top_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function pp_menus_header_top_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-header-top-menu_login:user/login.
  $menu_links['menu-header-top-menu_login:user/login'] = array(
    'menu_name' => 'menu-header-top-menu',
    'link_path' => 'user/login',
    'router_path' => 'user/login',
    'link_title' => 'Login',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-header-top-menu_login:user/login',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-header-top-menu_logout:user/logout.
  $menu_links['menu-header-top-menu_logout:user/logout'] = array(
    'menu_name' => 'menu-header-top-menu',
    'link_path' => 'user/logout',
    'router_path' => 'user/logout',
    'link_title' => 'Logout',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-header-top-menu_logout:user/logout',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-header-top-menu_media-gallery:admin/content/file.
  $menu_links['menu-header-top-menu_media-gallery:admin/content/file'] = array(
    'menu_name' => 'menu-header-top-menu',
    'link_path' => 'admin/content/file',
    'router_path' => 'admin/content/file',
    'link_title' => 'Media Gallery',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-header-top-menu_media-gallery:admin/content/file',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-header-top-menu_search:search/content.
  $menu_links['menu-header-top-menu_search:search/content'] = array(
    'menu_name' => 'menu-header-top-menu',
    'link_path' => 'search/content',
    'router_path' => 'search',
    'link_title' => 'Search',
    'options' => array(
      'menu_attach_block' => array(
        'name' => 'search|form',
        'use_ajax' => 0,
        'no_drop' => 0,
        'dropped' => 1,
        'on_hover' => 0,
        'orientation' => 'horizontal',
        'mlid' => 1402,
        'plid' => 0,
      ),
      'alter' => TRUE,
      'external' => 0,
      'attributes' => array(
        'class' => array(
          0 => 'search',
          1 => 'search-link',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => 'search-box',
        'style' => '',
      ),
      'identifier' => 'menu-header-top-menu_search:search/content',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Login');
  t('Logout');
  t('Media Gallery');
  t('Search');

  return $menu_links;
}
