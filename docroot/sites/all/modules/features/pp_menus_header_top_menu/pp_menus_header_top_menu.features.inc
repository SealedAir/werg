<?php
/**
 * @file
 * pp_menus_header_top_menu.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_menus_header_top_menu_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
