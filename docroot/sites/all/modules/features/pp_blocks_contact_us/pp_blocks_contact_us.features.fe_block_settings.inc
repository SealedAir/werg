<?php
/**
 * @file
 * pp_blocks_contact_us.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function pp_blocks_contact_us_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['webform-client-block-1'] = array(
    'cache' => -1,
    'css_class' => 'contact-us',
    'custom' => 0,
    'delta' => 'client-block-1',
    'i18n_block_language' => array(),
    'i18n_mode' => 1,
    'module' => 'webform',
    'node_types' => array(),
    'pages' => 'user/*
node/*/edit
node/add
node/add/*
admin
admin/*
file/*
node/*/delete',
    'roles' => array(
      'anonymous user' => 1,
      'authenticated user' => 2,
    ),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
      'seeportals' => array(
        'region' => 'footer_contact',
        'status' => 1,
        'theme' => 'seeportals',
        'weight' => -21,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
