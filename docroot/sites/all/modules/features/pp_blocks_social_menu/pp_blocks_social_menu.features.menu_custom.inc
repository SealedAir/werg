<?php
/**
 * @file
 * pp_blocks_social_menu.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function pp_blocks_social_menu_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-social-links.
  $menus['menu-social-links'] = array(
    'menu_name' => 'menu-social-links',
    'title' => 'Social links',
    'description' => 'The Social links menu contains the social media links displayed in the right side of the footer.',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Social links');
  t('The Social links menu contains the social media links displayed in the right side of the footer.');

  return $menus;
}
