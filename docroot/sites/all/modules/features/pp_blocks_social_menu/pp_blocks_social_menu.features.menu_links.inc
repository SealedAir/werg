<?php
/**
 * @file
 * pp_blocks_social_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function pp_blocks_social_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-social-links_facebook:https://www.facebook.com/pages/Sealed-Air-Corp/287944878104.
  $menu_links['menu-social-links_facebook:https://www.facebook.com/pages/Sealed-Air-Corp/287944878104'] = array(
    'menu_name' => 'menu-social-links',
    'link_path' => 'https://www.facebook.com/pages/Sealed-Air-Corp/287944878104',
    'router_path' => '',
    'link_title' => 'Facebook',
    'options' => array(
      'alter' => TRUE,
      'external' => 1,
      'attributes' => array(
        'class' => array(
          0 => 'facebook',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-social-links_facebook:https://www.facebook.com/pages/Sealed-Air-Corp/287944878104',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-social-links_linkedin:https://www.linkedin.com/company/sealed-air-corporation.
  $menu_links['menu-social-links_linkedin:https://www.linkedin.com/company/sealed-air-corporation'] = array(
    'menu_name' => 'menu-social-links',
    'link_path' => 'https://www.linkedin.com/company/sealed-air-corporation',
    'router_path' => '',
    'link_title' => 'Linkedin',
    'options' => array(
      'alter' => TRUE,
      'external' => 1,
      'attributes' => array(
        'class' => array(
          0 => 'linkedin',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-social-links_linkedin:https://www.linkedin.com/company/sealed-air-corporation',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-social-links_twitter:https://twitter.com/Sealed_Air.
  $menu_links['menu-social-links_twitter:https://twitter.com/Sealed_Air'] = array(
    'menu_name' => 'menu-social-links',
    'link_path' => 'https://twitter.com/Sealed_Air',
    'router_path' => '',
    'link_title' => 'Twitter',
    'options' => array(
      'alter' => TRUE,
      'external' => 1,
      'attributes' => array(
        'id' => 'twitter',
        'class' => array(
          0 => 'twitter',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-social-links_twitter:https://twitter.com/Sealed_Air',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-social-links_youtube:http://www.youtube.com/user/SealedAirCorp.
  $menu_links['menu-social-links_youtube:http://www.youtube.com/user/SealedAirCorp'] = array(
    'menu_name' => 'menu-social-links',
    'link_path' => 'http://www.youtube.com/user/SealedAirCorp',
    'router_path' => '',
    'link_title' => 'Youtube',
    'options' => array(
      'alter' => TRUE,
      'external' => 1,
      'attributes' => array(
        'class' => array(
          0 => 'youtube',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-social-links_youtube:http://www.youtube.com/user/SealedAirCorp',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Facebook');
  t('Linkedin');
  t('Twitter');
  t('Youtube');

  return $menu_links;
}
