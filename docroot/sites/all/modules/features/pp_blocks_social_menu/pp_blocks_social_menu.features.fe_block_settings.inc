<?php
/**
 * @file
 * pp_blocks_social_menu.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function pp_blocks_social_menu_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['menu-menu-social-links'] = array(
    'cache' => -1,
    'css_class' => 'bottom-menu social-links',
    'custom' => 0,
    'delta' => 'menu-social-links',
    'i18n_block_language' => array(),
    'i18n_mode' => 0,
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'seeportals' => array(
        'region' => 'sub_footer',
        'status' => 1,
        'theme' => 'seeportals',
        'weight' => -22,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
