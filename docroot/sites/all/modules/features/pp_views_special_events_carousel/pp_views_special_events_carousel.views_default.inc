<?php
/**
 * @file
 * pp_views_special_events_carousel.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function pp_views_special_events_carousel_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'special_events_carousel';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Special events carousel';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Events';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'view all events';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'events view access';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'slick';
  $handler->display->display_options['style_options']['slide_field_wrapper'] = 0;
  $handler->display->display_options['style_options']['asnavfor_auto'] = 0;
  $handler->display->display_options['style_options']['optionset'] = 'slick_special_events';
  $handler->display->display_options['style_options']['skin'] = 'default';
  $handler->display->display_options['style_options']['slide_caption'] = array(
    'path' => 0,
    'field_image_node' => 0,
    'title' => 0,
    'body' => 0,
    'view_node' => 0,
  );
  $handler->display->display_options['style_options']['slide_overlay'] = '';
  $handler->display->display_options['style_options']['grid_medium'] = '2';
  $handler->display->display_options['style_options']['grid_small'] = '1';
  $handler->display->display_options['style_options']['visible_slides'] = '3';
  $handler->display->display_options['style_options']['preserve_keys'] = 0;
  $handler->display->display_options['style_options']['override'] = 0;
  $handler->display->display_options['style_options']['overridables'] = array(
    'arrows' => 0,
    'autoplay' => 0,
    'dots' => 0,
    'draggable' => 0,
  );
  $handler->display->display_options['style_options']['cache'] = '';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<p>There are no events published. To add an Event click <a href="/node/add/event" >here</a>.<p>';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['max_length'] = '27';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_special_date']['id'] = 'field_special_date';
  $handler->display->display_options['fields']['field_special_date']['table'] = 'field_data_field_special_date';
  $handler->display->display_options['fields']['field_special_date']['field'] = 'field_special_date';
  $handler->display->display_options['fields']['field_special_date']['label'] = '';
  $handler->display->display_options['fields']['field_special_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_special_date']['settings'] = array(
    'format_type' => 'special_events',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Content: Image (700x340) */
  $handler->display->display_options['fields']['field_image_node']['id'] = 'field_image_node';
  $handler->display->display_options['fields']['field_image_node']['table'] = 'field_data_field_image_node';
  $handler->display->display_options['fields']['field_image_node']['field'] = 'field_image_node';
  $handler->display->display_options['fields']['field_image_node']['label'] = '';
  $handler->display->display_options['fields']['field_image_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image_node']['empty'] = '<img class="img-responsive" typeof="foaf:Image" src="sites/all/themes/seeportals/images/ghostimage_events.jpg" width="400" height="165">';
  $handler->display->display_options['fields']['field_image_node']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_image_node']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image_node']['settings'] = array(
    'file_view_mode' => 'special_events',
  );
  /* Field: Content: Description */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['max_length'] = '128';
  $handler->display->display_options['fields']['body']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_plain';
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['view_node']['alter']['text'] = 'Learn more';
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  /* Field: Content: External link */
  $handler->display->display_options['fields']['field_external_link_events']['id'] = 'field_external_link_events';
  $handler->display->display_options['fields']['field_external_link_events']['table'] = 'field_data_field_external_link_events';
  $handler->display->display_options['fields']['field_external_link_events']['field'] = 'field_external_link_events';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'event' => 'event',
  );
  /* Filter criterion: Content: Language */
  $handler->display->display_options['filters']['language']['id'] = 'language';
  $handler->display->display_options['filters']['language']['table'] = 'node';
  $handler->display->display_options['filters']['language']['field'] = 'language';
  $handler->display->display_options['filters']['language']['value'] = array(
    '***CURRENT_LANGUAGE***' => '***CURRENT_LANGUAGE***',
    '***DEFAULT_LANGUAGE***' => '***DEFAULT_LANGUAGE***',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['footer'] = FALSE;
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['content'] = '<div class="more-link">
<?php print l(\'View all events\',\'events\');?>
</div>';
  $handler->display->display_options['footer']['area']['format'] = 'php_code';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = 'aaaaaaaa';
  $handler->display->display_options['fields']['title']['alter']['max_length'] = '25';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_special_date']['id'] = 'field_special_date';
  $handler->display->display_options['fields']['field_special_date']['table'] = 'field_data_field_special_date';
  $handler->display->display_options['fields']['field_special_date']['field'] = 'field_special_date';
  $handler->display->display_options['fields']['field_special_date']['label'] = '';
  $handler->display->display_options['fields']['field_special_date']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_special_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_special_date']['settings'] = array(
    'format_type' => 'special_events',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Content: Image (700x340) */
  $handler->display->display_options['fields']['field_image_node']['id'] = 'field_image_node';
  $handler->display->display_options['fields']['field_image_node']['table'] = 'field_data_field_image_node';
  $handler->display->display_options['fields']['field_image_node']['field'] = 'field_image_node';
  $handler->display->display_options['fields']['field_image_node']['label'] = '';
  $handler->display->display_options['fields']['field_image_node']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_image_node']['alter']['text'] = 'Asasasa';
  $handler->display->display_options['fields']['field_image_node']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_image_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image_node']['empty'] = '<img class="img-responsive" typeof="foaf:Image" src="sites/all/themes/seeportals/images/ghostimage_events.jpg" width="400" height="165">';
  $handler->display->display_options['fields']['field_image_node']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_image_node']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image_node']['settings'] = array(
    'file_view_mode' => 'special_events',
  );
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="slide_element">
<div class="slide_title">[title]</div>
<div class="slide_date">[field_special_date]</div>
[field_image_node]
</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Field: Content: Description */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['max_length'] = '128';
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['exclude'] = TRUE;
  $handler->display->display_options['fields']['view_node']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['view_node']['alter']['text'] = 'Learn more';
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  /* Field: Content: External link */
  $handler->display->display_options['fields']['field_external_link_events']['id'] = 'field_external_link_events';
  $handler->display->display_options['fields']['field_external_link_events']['table'] = 'field_data_field_external_link_events';
  $handler->display->display_options['fields']['field_external_link_events']['field'] = 'field_external_link_events';
  $handler->display->display_options['fields']['field_external_link_events']['label'] = '';
  $handler->display->display_options['fields']['field_external_link_events']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_external_link_events']['alter']['text'] = '<a href=[field_external_link_events-url] target="_blank">Read more</a>';
  $handler->display->display_options['fields']['field_external_link_events']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_external_link_events']['empty'] = '[view_node]';
  $handler->display->display_options['fields']['field_external_link_events']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_external_link_events']['type'] = 'link_absolute';
  $translatables['special_events_carousel'] = array(
    t('Master'),
    t('Events'),
    t('view all events'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('<p>There are no events published. To add an Event click <a href="/node/add/event" >here</a>.<p>'),
    t('<img class="img-responsive" typeof="foaf:Image" src="sites/all/themes/seeportals/images/ghostimage_events.jpg" width="400" height="165">'),
    t('Learn more'),
    t('External link'),
    t('Block'),
    t('more'),
    t('<div class="more-link">
<?php print l(\'View all events\',\'events\');?>
</div>'),
    t('aaaaaaaa'),
    t('Asasasa'),
    t('<div class="slide_element">
<div class="slide_title">[title]</div>
<div class="slide_date">[field_special_date]</div>
[field_image_node]
</div>'),
    t('<a href=[field_external_link_events-url] target="_blank">Read more</a>'),
    t('[view_node]'),
  );
  $export['special_events_carousel'] = $view;

  return $export;
}
