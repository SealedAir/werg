<?php
/**
 * @file
 * pp_views_special_events_carousel.features.inc
 */

/**
 * Implements hook_views_api().
 */
function pp_views_special_events_carousel_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
