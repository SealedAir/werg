<?php
/**
 * @file
 * pp_blocks_copyright.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function pp_blocks_copyright_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Copyright';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'block_copyright';
  $fe_block_boxes->body = '<p>Copyright @2016&nbsp;Sealed Air&nbsp;</p>
';

  $export['block_copyright'] = $fe_block_boxes;

  return $export;
}
