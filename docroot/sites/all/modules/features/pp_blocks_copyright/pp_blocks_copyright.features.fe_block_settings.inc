<?php
/**
 * @file
 * pp_blocks_copyright.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function pp_blocks_copyright_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-block_copyright'] = array(
    'cache' => -1,
    'css_class' => 'copyright',
    'custom' => 0,
    'i18n_block_language' => array(),
    'i18n_mode' => 1,
    'machine_name' => 'block_copyright',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
      'seeportals' => array(
        'region' => 'sub_footer',
        'status' => 1,
        'theme' => 'seeportals',
        'weight' => -22,
      ),
    ),
    'title' => '',
    'visibility' => 0,
    'edit-role-view-settings' => array(
      'roles' => array(
        'Super-editor' => 'Super-editor',
        'administrator' => 'administrator',
      ),
     ),
  );

  return $export;
}
