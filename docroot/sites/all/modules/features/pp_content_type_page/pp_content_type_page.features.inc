<?php
/**
 * @file
 * pp_content_type_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_content_type_page_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function pp_content_type_page_node_info() {
  $items = array(
    'dvc_page' => array(
      'name' => t('Page'),
      'base' => 'node_content',
      'description' => t('Create a static page with image and file attachment.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
