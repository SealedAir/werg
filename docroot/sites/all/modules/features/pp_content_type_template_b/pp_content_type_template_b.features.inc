<?php
/**
 * @file
 * pp_content_type_template_b.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_content_type_template_b_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function pp_content_type_template_b_node_info() {
  $items = array(
    'template_b' => array(
      'name' => t('Template B'),
      'base' => 'node_content',
      'description' => t('Add a Template B page through which information can be promoted 
      using a different layout.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
