<?php
/**
 * @file
 * pp_content_type_news.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_content_type_news_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function pp_content_type_news_node_info() {
  $items = array(
    'news' => array(
      'name' => t('News'),
      'base' => 'node_content',
      'description' => t('Add news content through which you can promote news or 
      other information of interest on both the front page and the dedicated news 
      list page.Photos or attachments can be added along the description.
      The content can point to both the created content or an external link 
      (e.g. source of the content).'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
