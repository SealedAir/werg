<?php
/**
 * @file
 * pp_image_styles.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function pp_image_styles_image_default_styles() {
  $styles = array();

  // Exported image style: footer_image_template.
  $styles['footer_image_template'] = array(
    'label' => 'Footer image template',
    'effects' => array(
      9 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 250,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: image_description_template.
  $styles['image_description_template'] = array(
    'label' => 'Image description template',
    'effects' => array(
      10 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 285,
          'height' => 150,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: news-user.
  $styles['news-user'] = array(
    'label' => 'news-user',
    'effects' => array(
      11 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 55,
          'height' => 55,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: news_node.
  $styles['news_node'] = array(
    'label' => 'news_node',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 281,
          'height' => 175,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: partner_logo.
  $styles['partner_logo'] = array(
    'label' => 'Partner logo',
    'effects' => array(
      32 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => 70,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: promotion_node_slider.
  $styles['promotion_node_slider'] = array(
    'label' => 'Promotion node slider',
    'effects' => array(
      16 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => 450,
          'upscale' => 0,
        ),
        'weight' => -10,
      ),
      26 => array(
        'name' => 'canvasactions_definecanvas',
        'data' => array(
          'RGB' => array(
            'HEX' => '#eeeeee',
          ),
          'under' => 1,
          'exact' => array(
            'width' => 1200,
            'height' => 450,
            'xpos' => 'center',
            'ypos' => 'center',
          ),
          'relative' => array(
            'leftdiff' => '',
            'rightdiff' => '',
            'topdiff' => '',
            'bottomdiff' => '',
          ),
        ),
        'weight' => -9,
      ),
      21 => array(
        'name' => 'image_crop',
        'data' => array(
          'width' => 1200,
          'height' => 450,
          'anchor' => 'center-center',
        ),
        'weight' => -8,
      ),
    ),
  );

  // Exported image style: promotions_list.
  $styles['promotions_list'] = array(
    'label' => 'promotions_list',
    'effects' => array(
      6 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 700,
          'height' => 340,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: special_events.
  $styles['special_events'] = array(
    'label' => 'special_events',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 400,
          'height' => 165,
        ),
        'weight' => -10,
      ),
      41 => array(
        'name' => 'canvasactions_file2canvas',
        'data' => array(
          'xpos' => -5,
          'ypos' => 0,
          'alpha' => 50,
          'scale' => 100,
          'path' => 'sites/all/themes/seeportals/images/grey.jpg',
        ),
        'weight' => -9,
      ),
    ),
  );

  // Exported image style: special_events_list.
  $styles['special_events_list'] = array(
    'label' => 'special_events_list',
    'effects' => array(
      42 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 400,
          'height' => 165,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: square_thumbnail.
  $styles['square_thumbnail'] = array(
    'label' => 'square_thumbnail',
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 100,
          'height' => 100,
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: template_footer_image.
  $styles['template_footer_image'] = array(
    'label' => 'template_footer_image',
    'effects' => array(
      7 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1035,
          'height' => 215,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: user_photo.
  $styles['user_photo'] = array(
    'label' => 'User photo',
    'effects' => array(
      31 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 385,
          'height' => 290,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
