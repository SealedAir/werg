<?php
/**
 * @file
 * pp_breakpoints_tablet.default_breakpoints.inc
 */

/**
 * Implements hook_default_breakpoints().
 */
function pp_breakpoints_tablet_default_breakpoints() {
  $export = array();

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'custom.user.tablet';
  $breakpoint->name = 'Tablet';
  $breakpoint->breakpoint = '(min-width: 769px)|(max-width: 991px)';
  $breakpoint->source = 'user';
  $breakpoint->source_type = 'custom';
  $breakpoint->status = 1;
  $breakpoint->weight = 1;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['custom.user.tablet'] = $breakpoint;

  return $export;
}
