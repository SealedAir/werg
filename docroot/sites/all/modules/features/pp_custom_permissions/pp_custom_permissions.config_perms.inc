<?php
/**
 * @file
 * pp_custom_permissions.config_perms.inc
 */

/**
 * Implements hook_config_perms().
 */
function pp_custom_permissions_config_perms() {
  $export = array();

  $config_perms = new stdClass();
  $config_perms->disabled = FALSE; /* Edit this to true to make a default config_perms disabled initially */
  $config_perms->api_version = 1;
  $config_perms->machine_name = 'access_import';
  $config_perms->status = TRUE;
  $config_perms->name = 'access import';
  $config_perms->path = array(
    0 => 'import',
  );
  $export['access_import'] = $config_perms;

  $config_perms = new stdClass();
  $config_perms->disabled = FALSE; /* Edit this to true to make a default config_perms disabled initially */
  $config_perms->api_version = 1;
  $config_perms->machine_name = 'access_seemenu';
  $config_perms->status = TRUE;
  $config_perms->name = 'access seemenu';
  $config_perms->path = array(
    0 => 'admin/seemenu',
  );
  $export['access_seemenu'] = $config_perms;

  $config_perms = new stdClass();
  $config_perms->disabled = FALSE; /* Edit this to true to make a default config_perms disabled initially */
  $config_perms->api_version = 1;
  $config_perms->machine_name = 'access_seemenu_cenabling';
  $config_perms->status = TRUE;
  $config_perms->name = 'access seemenu cenabling';
  $config_perms->path = array(
    0 => 'admin/seemenu/content-enabling',
  );
  $export['access_seemenu_cenabling'] = $config_perms;

  $config_perms = new stdClass();
  $config_perms->disabled = FALSE; /* Edit this to true to make a default config_perms disabled initially */
  $config_perms->api_version = 1;
  $config_perms->machine_name = 'access_seemenu_logo';
  $config_perms->status = TRUE;
  $config_perms->name = 'access seemenu logo';
  $config_perms->path = array(
    0 => 'admin/seemenu/logo',
  );
  $export['access_seemenu_logo'] = $config_perms;

  $config_perms = new stdClass();
  $config_perms->disabled = FALSE; /* Edit this to true to make a default config_perms disabled initially */
  $config_perms->api_version = 1;
  $config_perms->machine_name = 'manage_adminimal_menu';
  $config_perms->status = TRUE;
  $config_perms->name = 'manage adminimal menu';
  $config_perms->path = array(
    0 => 'admin/config/administration/adminimal_menu',
  );
  $export['manage_adminimal_menu'] = $config_perms;

  $config_perms = new stdClass();
  $config_perms->disabled = FALSE; /* Edit this to true to make a default config_perms disabled initially */
  $config_perms->api_version = 1;
  $config_perms->machine_name = 'manage_administration_menu';
  $config_perms->status = TRUE;
  $config_perms->name = 'manage administration menu';
  $config_perms->path = array(
    0 => 'admin/config/administration/admin_menu',
  );
  $export['manage_administration_menu'] = $config_perms;

  $config_perms = new stdClass();
  $config_perms->disabled = FALSE; /* Edit this to true to make a default config_perms disabled initially */
  $config_perms->api_version = 1;
  $config_perms->machine_name = 'manage_content_enabling';
  $config_perms->status = TRUE;
  $config_perms->name = 'manage content enabling';
  $config_perms->path = array(
    0 => 'admin/config/system/content-enabling',
  );
  $export['manage_content_enabling'] = $config_perms;

  $config_perms = new stdClass();
  $config_perms->disabled = FALSE; /* Edit this to true to make a default config_perms disabled initially */
  $config_perms->api_version = 1;
  $config_perms->machine_name = 'manage_date_popup';
  $config_perms->status = TRUE;
  $config_perms->name = 'manage date popup';
  $config_perms->path = array(
    0 => 'admin/config/date/date_popup',
  );
  $export['manage_date_popup'] = $config_perms;

  $config_perms = new stdClass();
  $config_perms->disabled = FALSE; /* Edit this to true to make a default config_perms disabled initially */
  $config_perms->api_version = 1;
  $config_perms->machine_name = 'manage_entity_panels';
  $config_perms->status = TRUE;
  $config_perms->name = 'manage entity panels';
  $config_perms->path = array(
    0 => 'admin/config/content/entity_panels',
  );
  $export['manage_entity_panels'] = $config_perms;

  $config_perms = new stdClass();
  $config_perms->disabled = FALSE; /* Edit this to true to make a default config_perms disabled initially */
  $config_perms->api_version = 1;
  $config_perms->machine_name = 'manage_hacked';
  $config_perms->status = TRUE;
  $config_perms->name = 'manage hacked';
  $config_perms->path = array(
    0 => 'admin/reports/hacked',
  );
  $export['manage_hacked'] = $config_perms;

  $config_perms = new stdClass();
  $config_perms->disabled = FALSE; /* Edit this to true to make a default config_perms disabled initially */
  $config_perms->api_version = 1;
  $config_perms->machine_name = 'manage_jquery';
  $config_perms->status = TRUE;
  $config_perms->name = 'manage jquery';
  $config_perms->path = array(
    0 => 'admin/config/development/jquery_update',
  );
  $export['manage_jquery'] = $config_perms;

  $config_perms = new stdClass();
  $config_perms->disabled = FALSE; /* Edit this to true to make a default config_perms disabled initially */
  $config_perms->api_version = 1;
  $config_perms->machine_name = 'manage_strongarm';
  $config_perms->status = TRUE;
  $config_perms->name = 'manage strongarm';
  $config_perms->path = array(
    0 => 'admin/config/development/strongarm',
  );
  $export['manage_strongarm'] = $config_perms;

  $config_perms = new stdClass();
  $config_perms->disabled = FALSE; /* Edit this to true to make a default config_perms disabled initially */
  $config_perms->api_version = 1;
  $config_perms->machine_name = 'manage_uuid_features';
  $config_perms->status = TRUE;
  $config_perms->name = 'manage uuid features';
  $config_perms->path = array(
    0 => 'admin/config/content/uuid_features',
  );
  $export['manage_uuid_features'] = $config_perms;

  $config_perms = new stdClass();
  $config_perms->disabled = FALSE; /* Edit this to true to make a default config_perms disabled initially */
  $config_perms->api_version = 1;
  $config_perms->machine_name = 'remove_manage_display';
  $config_perms->status = TRUE;
  $config_perms->name = 'remove manage display';
  $config_perms->path = array(
    0 => 'user/*/display',
  );
  $export['remove_manage_display'] = $config_perms;

  return $export;
}
