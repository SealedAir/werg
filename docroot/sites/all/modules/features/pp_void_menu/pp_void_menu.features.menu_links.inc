<?php
/**
 * @file
 * pp_void_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function pp_void_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management_void-menu:admin/config/user-interface/void_menu.
  $menu_links['management_void-menu:admin/config/user-interface/void_menu'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/config/user-interface/void_menu',
    'router_path' => 'admin/config/user-interface/void_menu',
    'link_title' => 'Void Menu',
    'options' => array(
      'attributes' => array(
        'title' => 'Configure Void Menu.',
      ),
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'management_void-menu:admin/config/user-interface/void_menu',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
    'parent_identifier' => 'management_user-interface:admin/config/user-interface',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Void Menu');

  return $menu_links;
}
