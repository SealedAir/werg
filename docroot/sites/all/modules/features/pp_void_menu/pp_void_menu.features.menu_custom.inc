<?php
/**
 * @file
 * pp_void_menu.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function pp_void_menu_menu_default_menu_custom() {
  $menus = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Management');
  t('The <em>Management</em> menu contains links for administrative tasks.');

  return $menus;
}
