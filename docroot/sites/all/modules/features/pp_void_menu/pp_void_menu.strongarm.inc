<?php
/**
 * @file
 * pp_void_menu.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function pp_void_menu_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'void_menu_link_value';
  $strongarm->value = 'javascript: void(0);';
  $export['void_menu_link_value'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'void_menu_link_value1';
  $strongarm->value = '/#events';
  $export['void_menu_link_value1'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'void_menu_link_value2';
  $strongarm->value = '#contactus';
  $export['void_menu_link_value2'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'void_menu_link_value3';
  $strongarm->value = '/#news';
  $export['void_menu_link_value3'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'void_menu_link_value4';
  $strongarm->value = '';
  $export['void_menu_link_value4'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'void_menu_link_value5';
  $strongarm->value = '';
  $export['void_menu_link_value5'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'void_menu_link_value6';
  $strongarm->value = '';
  $export['void_menu_link_value6'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'void_menu_link_value7';
  $strongarm->value = '';
  $export['void_menu_link_value7'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'void_menu_link_value8';
  $strongarm->value = '';
  $export['void_menu_link_value8'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'void_menu_link_value9';
  $strongarm->value = '';
  $export['void_menu_link_value9'] = $strongarm;

  return $export;
}
