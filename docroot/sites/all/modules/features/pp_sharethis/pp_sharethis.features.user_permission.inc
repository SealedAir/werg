<?php
/**
 * @file
 * pp_sharethis.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function pp_sharethis_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer sharethis'.
  $permissions['administer sharethis'] = array(
    'name' => 'administer sharethis',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'sharethis',
  );

  return $permissions;
}
