<?php
/**
 * @file
 * pp_sharethis.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function pp_sharethis_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management_sharethis:admin/config/services/sharethis.
  $menu_links['management_sharethis:admin/config/services/sharethis'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/config/services/sharethis',
    'router_path' => 'admin/config/services/sharethis',
    'link_title' => 'ShareThis',
    'options' => array(
      'attributes' => array(
        'title' => 'Choose the widget, button family, and services for using ShareThis to share content online.',
      ),
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'management_sharethis:admin/config/services/sharethis',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
    'parent_identifier' => 'management_web-services:admin/config/services',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('ShareThis');

  return $menu_links;
}
