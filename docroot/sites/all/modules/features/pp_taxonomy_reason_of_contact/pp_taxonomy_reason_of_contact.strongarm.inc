<?php
/**
 * @file
 * pp_taxonomy_reason_of_contact.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function pp_taxonomy_reason_of_contact_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_reason_of_contact_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_reason_of_contact_pattern'] = $strongarm;

  return $export;
}
