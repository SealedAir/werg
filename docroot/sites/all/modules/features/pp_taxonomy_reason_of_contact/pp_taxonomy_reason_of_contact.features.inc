<?php
/**
 * @file
 * pp_taxonomy_reason_of_contact.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_taxonomy_reason_of_contact_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
