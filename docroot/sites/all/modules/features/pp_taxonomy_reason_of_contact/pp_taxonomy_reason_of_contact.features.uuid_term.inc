<?php
/**
 * @file
 * pp_taxonomy_reason_of_contact.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function pp_taxonomy_reason_of_contact_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Business Development',
    'description' => '',
    'format' => 'full_html',
    'weight' => 3,
    'uuid' => '1558d07c-6469-4274-b1a4-c11e86ef6e0b',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'reason_of_contact',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Product Support',
    'description' => '',
    'format' => 'full_html',
    'weight' => 1,
    'uuid' => 'c2d0df77-be16-4f85-9a7a-081e91ddf9ea',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'reason_of_contact',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Sales',
    'description' => '',
    'format' => 'full_html',
    'weight' => 2,
    'uuid' => 'f9fd4a66-1213-4bb2-8ac7-a574f843edd9',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'reason_of_contact',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Careers',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => 'fd64ee54-4c41-4d81-b8aa-9a0ca1693ab1',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'reason_of_contact',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  return $terms;
}
