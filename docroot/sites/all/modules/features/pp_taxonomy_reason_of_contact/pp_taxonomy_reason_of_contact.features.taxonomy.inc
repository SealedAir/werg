<?php
/**
 * @file
 * pp_taxonomy_reason_of_contact.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function pp_taxonomy_reason_of_contact_taxonomy_default_vocabularies() {
  return array(
    'reason_of_contact' => array(
      'name' => 'Reason of Contact',
      'machine_name' => 'reason_of_contact',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 1,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
