<?php
/**
 * @file
 * pp_blocks_customer_testimonial.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function pp_blocks_customer_testimonial_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Customers testimonial block';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'block_homepage_customers';
  $fe_block_boxes->body = '<p>This is the body of the testimonial block. You can edit this content to show your custom testimonial text.This is a placeholder.</p>
';

  $export['block_homepage_customers'] = $fe_block_boxes;

  return $export;
}
