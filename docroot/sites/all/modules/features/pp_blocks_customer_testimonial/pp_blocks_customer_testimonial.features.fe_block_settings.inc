<?php
/**
 * @file
 * pp_blocks_customer_testimonial.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function pp_blocks_customer_testimonial_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-block_homepage_customers'] = array(
    'cache' => -1,
    'css_class' => 'testimonial-block',
    'custom' => 0,
    'i18n_block_language' => array(),
    'i18n_mode' => 1,
    'machine_name' => 'block_homepage_customers',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'seeportals' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'seeportals',
        'weight' => -19,
      ),
    ),
    'title' => 'Testimonial',
    'visibility' => 1,
  );

  return $export;
}
