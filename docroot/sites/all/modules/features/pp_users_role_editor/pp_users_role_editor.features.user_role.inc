<?php
/**
 * @file
 * pp_users_role_editor.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function pp_users_role_editor_user_default_roles() {
  $roles = array();

  // Exported role: Editor.
  $roles['Editor'] = array(
    'name' => 'Editor',
    'weight' => 3,
  );

  return $roles;
}
