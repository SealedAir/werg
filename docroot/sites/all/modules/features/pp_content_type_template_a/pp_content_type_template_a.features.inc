<?php
/**
 * @file
 * pp_content_type_template_a.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_content_type_template_a_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function pp_content_type_template_a_node_info() {
  $items = array(
    'template_a' => array(
      'name' => t('Template A'),
      'base' => 'node_content',
      'description' => t('Add a Template A page through which information can be promoted 
      using a different layout.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
