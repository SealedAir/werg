<?php
/**
 * @file
 * pp_content_type_partner.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_content_type_partner_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function pp_content_type_partner_node_info() {
  $items = array(
    'partner' => array(
      'name' => t('Partner'),
      'base' => 'node_content',
      'description' => t('Add a partner logo (plus an optional link) to be displayed on the website.
      A list of partners can be added but only the front page promoted ones will be shown on the front page.'),
      'has_title' => '1',
      'title_label' => t('Partner name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
