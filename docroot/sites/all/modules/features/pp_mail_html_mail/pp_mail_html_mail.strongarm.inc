<?php
/**
 * @file
 * pp_mail_html_mail.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function pp_mail_html_mail_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'htmlmail_postfilter';
  $strongarm->value = 'full_html';
  $export['htmlmail_postfilter'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'htmlmail_theme';
  $strongarm->value = 'seeportals';
  $export['htmlmail_theme'] = $strongarm;

  return $export;
}
