<?php
/**
 * @file
 * pp_webform_placeholder.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function pp_webform_placeholder_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management_form-placeholder:admin/config/user-interface/form-placeholder.
  $menu_links['management_form-placeholder:admin/config/user-interface/form-placeholder'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/config/user-interface/form-placeholder',
    'router_path' => 'admin/config/user-interface/form-placeholder',
    'link_title' => 'Form Placeholder',
    'options' => array(
      'attributes' => array(
        'title' => 'Use field label as placeholder for all elements in choosen form.',
      ),
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'management_form-placeholder:admin/config/user-interface/form-placeholder',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
    'parent_identifier' => 'management_user-interface:admin/config/user-interface',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Form Placeholder');

  return $menu_links;
}
