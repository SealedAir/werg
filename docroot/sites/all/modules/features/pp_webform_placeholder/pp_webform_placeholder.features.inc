<?php
/**
 * @file
 * pp_webform_placeholder.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_webform_placeholder_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
