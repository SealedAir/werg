<?php
/**
 * @file
 * pp_webform_placeholder.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function pp_webform_placeholder_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer form placeholder'.
  $permissions['administer form placeholder'] = array(
    'name' => 'administer form placeholder',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'form_placeholder',
  );

  return $permissions;
}
