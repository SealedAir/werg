<?php
/**
 * @file
 * pp_views_testimonials.features.inc
 */

/**
 * Implements hook_views_api().
 */
function pp_views_testimonials_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
