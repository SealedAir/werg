<?php
/**
 * @file
 * pp_menus_footer_menu_bottom.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_menus_footer_menu_bottom_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
