<?php
/**
 * @file
 * pp_menus_footer_menu_bottom.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function pp_menus_footer_menu_bottom_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-footer-menu-bottom_code-of-conduct:http://www.sealedair.com/code-conduct.
  $menu_links['menu-footer-menu-bottom_code-of-conduct:http://www.sealedair.com/code-conduct'] = array(
    'menu_name' => 'menu-footer-menu-bottom',
    'link_path' => 'http://www.sealedair.com/code-conduct',
    'router_path' => '',
    'link_title' => 'Code of Conduct',
    'options' => array(
      'alter' => TRUE,
      'external' => 1,
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-footer-menu-bottom_code-of-conduct:http://www.sealedair.com/code-conduct',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-footer-menu-bottom_privacy:http://www.sealedair.com/privacy-policy.
  $menu_links['menu-footer-menu-bottom_privacy:http://www.sealedair.com/privacy-policy'] = array(
    'menu_name' => 'menu-footer-menu-bottom',
    'link_path' => 'http://www.sealedair.com/privacy-policy',
    'router_path' => '',
    'link_title' => 'Privacy',
    'options' => array(
      'alter' => TRUE,
      'external' => 1,
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-footer-menu-bottom_privacy:http://www.sealedair.com/privacy-policy',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-footer-menu-bottom_terms--conditions:http://www.sealedair.com/terms-conditions.
  $menu_links['menu-footer-menu-bottom_terms--conditions:http://www.sealedair.com/terms-conditions'] = array(
    'menu_name' => 'menu-footer-menu-bottom',
    'link_path' => 'http://www.sealedair.com/terms-conditions',
    'router_path' => '',
    'link_title' => 'Terms & Conditions',
    'options' => array(
      'alter' => TRUE,
      'external' => 1,
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-footer-menu-bottom_terms--conditions:http://www.sealedair.com/terms-conditions',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Code of Conduct');
  t('Privacy');
  t('Terms & Conditions');

  return $menu_links;
}
