<?php
/**
 * @file
 * pp_menus_footer_menu_bottom.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function pp_menus_footer_menu_bottom_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-footer-menu-bottom.
  $menus['menu-footer-menu-bottom'] = array(
    'menu_name' => 'menu-footer-menu-bottom',
    'title' => 'Footer menu bottom',
    'description' => 'The Footer menu bottom contains the links displayed in the footer. Related to "Privacy" or "Terms & Conditions".',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Footer menu bottom');
  t('The Footer menu bottom contains the links displayed in the footer. Related to "Privacy" or "Terms & Conditions".');

  return $menus;
}
