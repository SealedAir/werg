<?php
/**
 * @file
 * pp_display_suite.ds.inc
 */

/**
 * Implements hook_ds_view_modes_info().
 */
function pp_display_suite_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'flexslider_full';
  $ds_view_mode->label = 'flexslider_full';
  $ds_view_mode->entities = array(
    'file' => 'file',
  );
  $export['flexslider_full'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'flexslider_thumbnail';
  $ds_view_mode->label = 'flexslider_thumbnail';
  $ds_view_mode->entities = array(
    'file' => 'file',
  );
  $export['flexslider_thumbnail'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'footer_image_template_';
  $ds_view_mode->label = 'Footer image template ';
  $ds_view_mode->entities = array(
    'file' => 'file',
  );
  $export['footer_image_template_'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'image_description_template_';
  $ds_view_mode->label = 'Image description template ';
  $ds_view_mode->entities = array(
    'file' => 'file',
  );
  $export['image_description_template_'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'news_node';
  $ds_view_mode->label = 'news_node';
  $ds_view_mode->entities = array(
    'file' => 'file',
  );
  $export['news_node'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'promotion_node_slider';
  $ds_view_mode->label = 'promotion_node_slider';
  $ds_view_mode->entities = array(
    'file' => 'file',
  );
  $export['promotion_node_slider'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'promotions_list';
  $ds_view_mode->label = 'promotions_list';
  $ds_view_mode->entities = array(
    'file' => 'file',
  );
  $export['promotions_list'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'special_events';
  $ds_view_mode->label = 'special_events';
  $ds_view_mode->entities = array(
    'file' => 'file',
  );
  $export['special_events'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'template_footer_image';
  $ds_view_mode->label = 'Template footer image';
  $ds_view_mode->entities = array(
    'file' => 'file',
  );
  $export['template_footer_image'] = $ds_view_mode;

  return $export;
}
