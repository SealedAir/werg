<?php
/**
 * @file
 * pp_sliders_slick_special_events.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_sliders_slick_special_events_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "slick" && $api == "slick_default_preset") {
    return array("version" => "1");
  }
}
