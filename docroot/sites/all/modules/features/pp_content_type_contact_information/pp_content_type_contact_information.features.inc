<?php
/**
 * @file
 * pp_content_type_contact_information.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_content_type_contact_information_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function pp_content_type_contact_information_node_info() {
  $items = array(
    'contact' => array(
      'name' => t('Contact Information'),
      'base' => 'node_content',
      'description' => t('Add contact information in the footer area (Address, Telephone, Map coordinates, etc.).
      The Map coordinates will set the coordinates of the map displayed in the footer.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
