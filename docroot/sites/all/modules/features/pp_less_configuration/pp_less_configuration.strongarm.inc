<?php
/**
 * @file
 * pp_less_configuration.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function pp_less_configuration_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'less_engine';
  $strongarm->value = 'less.php';
  $export['less_engine'] = $strongarm;

  return $export;
}
