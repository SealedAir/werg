<?php
/**
 * @file
 * pp_blocks_map.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function pp_blocks_map_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['partner_portal_block_custom_map-block_custom_map'] = array(
    'cache' => 1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'block_custom_map',
    'i18n_block_language' => array(),
    'i18n_mode' => 0,
    'module' => 'partner_portal_block_custom_map',
    'node_types' => array(),
    'pages' => 'user
user/*
node/*/edit
node/add
node/add/*
admin
admin/*
file/*
node/*/delete',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
      'seeportals' => array(
        'region' => 'footer_contact',
        'status' => 1,
        'theme' => 'seeportals',
        'weight' => -20,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
