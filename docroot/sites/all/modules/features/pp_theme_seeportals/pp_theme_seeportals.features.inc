<?php
/**
 * @file
 * pp_theme_seeportals.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_theme_seeportals_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
