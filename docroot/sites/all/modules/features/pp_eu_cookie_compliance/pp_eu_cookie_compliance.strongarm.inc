<?php
/**
 * @file
 * pp_eu_cookie_compliance.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function pp_eu_cookie_compliance_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'eu_cookie_compliance_domain';
  $strongarm->value = '';
  $export['eu_cookie_compliance_domain'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'eu_cookie_compliance_en';
  $strongarm->value = array(
    'popup_enabled' => 1,
    'popup_clicking_confirmation' => 1,
    'popup_position' => 1,
    'popup_agree_button_message' => 'OK, I agree',
    'popup_disagree_button_message' => 'Read more',
    'popup_info' => array(
      'value' => '<p>By using this site, you consent to the use of cookies for purposes including advertising and analytics. Read our Privacy Policy for details and options.</p>
',
      'format' => 'full_html',
    ),
    'popup_agreed_enabled' => 0,
    'popup_hide_agreed' => 0,
    'popup_find_more_button_message' => 'More info',
    'popup_hide_button_message' => 'Hide',
    'popup_agreed' => array(
      'value' => '<p>Thank you for accepting cookies</p>

<p>You can now hide this message or find out more about cookies.</p>
',
      'format' => 'full_html',
    ),
    'popup_link' => 'http://www.sealedair.com/privacy-policy',
    'popup_link_new_window' => 1,
    'popup_height' => '',
    'popup_width' => '100%',
    'popup_delay' => '1',
    'popup_bg_hex' => '008d9c',
    'popup_text_hex' => 'ffffff',
    'domains_option' => '1',
    'domains_list' => '',
    'exclude_paths' => '',
  );
  $export['eu_cookie_compliance_en'] = $strongarm;

  return $export;
}
