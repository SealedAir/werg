<?php
/**
 * @file
 * pp_blocks_testimonial.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function pp_blocks_testimonial_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-testimonial_slider-block'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'testimonial_slider-block',
    'i18n_block_language' => array(),
    'i18n_mode' => 0,
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'seeportals' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'seeportals',
        'weight' => -19,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
