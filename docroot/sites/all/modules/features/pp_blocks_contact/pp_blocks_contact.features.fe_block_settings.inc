<?php
/**
 * @file
 * pp_blocks_contact.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function pp_blocks_contact_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-contact-block'] = array(
    'cache' => -1,
    'css_class' => 'contact-region',
    'custom' => 0,
    'delta' => 'contact-block',
    'i18n_block_language' => array(),
    'i18n_mode' => 0,
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'user
user/*
node/*/edit
node/add
node/add/*
admin
admin/*
file/*
file/add
file/add/*
node/*/delete',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
      'seeportals' => array(
        'region' => 'footer_contact',
        'status' => 1,
        'theme' => 'seeportals',
        'weight' => -22,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
