<?php
/**
 * @file
 * pp_taxonomy_paragraph_styles.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function pp_taxonomy_paragraph_styles_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Image Right',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '82543c64-cd5e-459a-ae77-4c6ba79e7383',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'paragraph_styles',
    'field_class' => array(
      'und' => array(
        0 => array(
          'value' => 'image-right',
          'format' => NULL,
          'safe_value' => 'image-right',
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Image Full',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'ae00fe71-5c93-435f-9fb2-f637779f33c4',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'paragraph_styles',
    'field_class' => array(
      'und' => array(
        0 => array(
          'value' => 'image-full',
          'format' => NULL,
          'safe_value' => 'image-full',
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Image Left',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'b83e5b73-badd-4533-bf4f-118d75776cbf',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'paragraph_styles',
    'field_class' => array(
      'und' => array(
        0 => array(
          'value' => 'image-left',
          'format' => NULL,
          'safe_value' => 'image-left',
        ),
      ),
    ),
  );
  return $terms;
}
