<?php
/**
 * @file
 * pp_taxonomy_paragraph_styles.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function pp_taxonomy_paragraph_styles_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'taxonomy_term-paragraph_styles-field_class'.
  $field_instances['taxonomy_term-paragraph_styles-field_class'] = array(
    'bundle' => 'paragraph_styles',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_class',
    'label' => 'Class',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 31,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Class');

  return $field_instances;
}
