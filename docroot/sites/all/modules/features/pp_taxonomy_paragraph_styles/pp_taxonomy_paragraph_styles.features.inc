<?php
/**
 * @file
 * pp_taxonomy_paragraph_styles.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_taxonomy_paragraph_styles_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
