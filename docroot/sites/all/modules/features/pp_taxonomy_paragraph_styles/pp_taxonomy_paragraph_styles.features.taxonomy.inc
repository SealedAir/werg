<?php
/**
 * @file
 * pp_taxonomy_paragraph_styles.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function pp_taxonomy_paragraph_styles_taxonomy_default_vocabularies() {
  return array(
    'paragraph_styles' => array(
      'name' => 'Paragraph Styles',
      'machine_name' => 'paragraph_styles',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
