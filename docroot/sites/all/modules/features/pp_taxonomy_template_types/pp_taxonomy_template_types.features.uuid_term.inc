<?php
/**
 * @file
 * pp_taxonomy_template_types.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function pp_taxonomy_template_types_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Template B',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => '0fd10ad5-1182-499f-ab08-c4743b947e8b',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'template_types',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Template A',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => 'de1d4add-6e3e-4ef9-ad26-7fc6711fb39a',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'template_types',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  return $terms;
}
