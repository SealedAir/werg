<?php
/**
 * @file
 * pp_context.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function pp_context_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'desktop_slider_controls';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'breakpoint' => array(
      'values' => array(
        0 => 'custom.user.desktop',
      ),
      'options' => array(
        'autoreload' => 0,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-promoted_slider-block_2' => array(
          'module' => 'views',
          'delta' => 'promoted_slider-block_2',
          'region' => 'slider',
          'weight' => '-23',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['desktop_slider_controls'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'managecontent';
  $context->description = '';
  $context->tag = 'Metatag';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'admin/manage/content' => 'admin/manage/content',
      ),
    ),
  );
  $context->reactions = array(
    'metatag_context_reaction' => array(
      'metatags' => array(
        'und' => array(
          'title' => array(
            'value' => 'Manage Content | [site:name]',
          ),
        ),
      ),
      'metatag_admin' => 1,
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Metatag');
  $export['managecontent'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'media_gallery';
  $context->description = '';
  $context->tag = 'Metatag';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'admin/content/media/list' => 'admin/content/media/list',
        'admin/content/media/thumbnails' => 'admin/content/media/thumbnails',
        'admin/content/media/import' => 'admin/content/media/import',
      ),
    ),
  );
  $context->reactions = array(
    'metatag_context_reaction' => array(
      'metatags' => array(
        'und' => array(
          'title' => array(
            'value' => 'Media Gallery | [site:name]',
          ),
        ),
      ),
      'metatag_admin' => 1,
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Metatag');
  $export['media_gallery'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'mobile_login';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'breakpoint' => array(
      'values' => array(
        0 => 'custom.user.mobile',
      ),
      'options' => array(
        'autoreload' => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-6' => array(
          'module' => 'block',
          'delta' => '6',
          'region' => 'header_top',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['mobile_login'] = $context;

  return $export;
}
