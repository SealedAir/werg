<?php
/**
 * @file
 * pp_sliders_flexslider_controls_slider.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_sliders_flexslider_controls_slider_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
}
