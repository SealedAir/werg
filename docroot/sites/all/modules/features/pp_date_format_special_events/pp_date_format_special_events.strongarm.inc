<?php
/**
 * @file
 * pp_date_format_special_events.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function pp_date_format_special_events_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_special_events';
  $strongarm->value = 'F j, Y';
  $export['date_format_special_events'] = $strongarm;

  return $export;
}
