<?php
/**
 * @file
 * pp_date_format_special_events.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_date_format_special_events_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_fe_date_custom_date_formats().
 */
function pp_date_format_special_events_fe_date_custom_date_formats() {
  $custom_date_formats = array();
  $custom_date_formats['F j, Y'] = 'F j, Y';
  return $custom_date_formats;
}

/**
 * Implements hook_date_format_types().
 */
function pp_date_format_special_events_date_format_types() {
  $format_types = array();
  // Exported date format type: special_events
  $format_types['special_events'] = 'Special Events';
  return $format_types;
}
