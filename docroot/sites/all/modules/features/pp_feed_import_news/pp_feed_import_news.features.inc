<?php
/**
 * @file
 * pp_feed_import_news.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_feed_import_news_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
}
