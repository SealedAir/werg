<?php
/**
 * @file
 * pp_page_manager_news_node_view.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function pp_page_manager_news_node_view_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__news';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'News',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'news',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'news' => 'news',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'metatag_panels' => array(
      'enabled' => 0,
      'metatags' => array(
        'title' => array(
          'value' => '',
        ),
      ),
    ),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'left',
          1 => 'center',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Right',
        'width' => 50,
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => 'col-lg-8 col-md-8 col-sm-8 col-xs-12',
        'hide_empty' => 0,
      ),
      'left' => array(
        'type' => 'region',
        'title' => 'Left',
        'width' => 50,
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => 'col-lg-4 col-md-4 col-sm-4 col-xs-12',
        'hide_empty' => 0,
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'left' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '790b3b12-d341-4f20-9a41-98f04fd197dc';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-3e351a02-ce4c-47e2-96dd-84090eb5df7d';
    $pane->panel = 'center';
    $pane->type = 'views_panes';
    $pane->subtype = 'news-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3e351a02-ce4c-47e2-96dd-84090eb5df7d';
    $display->content['new-3e351a02-ce4c-47e2-96dd-84090eb5df7d'] = $pane;
    $display->panels['center'][0] = 'new-3e351a02-ce4c-47e2-96dd-84090eb5df7d';
    $pane = new stdClass();
    $pane->pid = 'new-78016853-77d9-4388-a93e-8be88c1573fd';
    $pane->panel = 'center';
    $pane->type = 'sharethis';
    $pane->subtype = 'sharethis';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'path' => 'global',
      'path-external' => '',
      'override_title' => 1,
      'override_title_text' => 'SHARE ARTICLE ON',
      'override_title_heading' => 'h6',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'visible-xs',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '78016853-77d9-4388-a93e-8be88c1573fd';
    $display->content['new-78016853-77d9-4388-a93e-8be88c1573fd'] = $pane;
    $display->panels['center'][1] = 'new-78016853-77d9-4388-a93e-8be88c1573fd';
    $pane = new stdClass();
    $pane->pid = 'new-7e2c7753-423d-485b-976d-6eeac1a050d0';
    $pane->panel = 'left';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'media',
      'delta_limit' => '0',
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'formatter_settings' => array(
        'field_delimiter' => '',
        'file_view_mode' => 'news_node',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7e2c7753-423d-485b-976d-6eeac1a050d0';
    $display->content['new-7e2c7753-423d-485b-976d-6eeac1a050d0'] = $pane;
    $display->panels['left'][0] = 'new-7e2c7753-423d-485b-976d-6eeac1a050d0';
    $pane = new stdClass();
    $pane->pid = 'new-6d945bd6-2b54-4265-b919-19135c5de594';
    $pane->panel = 'left';
    $pane->type = 'sharethis';
    $pane->subtype = 'sharethis';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'path' => 'global',
      'path-external' => '',
      'override_title' => 1,
      'override_title_text' => 'SHARE ARTICLE ON',
      'override_title_heading' => 'h6',
      'breakpoints' => array(
        'custom-user-mobile' => 1,
        'custom-user-tablet' => 1,
        'custom-user-desktop' => 1,
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'hidden-xs',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '6d945bd6-2b54-4265-b919-19135c5de594';
    $display->content['new-6d945bd6-2b54-4265-b919-19135c5de594'] = $pane;
    $display->panels['left'][1] = 'new-6d945bd6-2b54-4265-b919-19135c5de594';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-3e351a02-ce4c-47e2-96dd-84090eb5df7d';
  $handler->conf['display'] = $display;
  $export['node_view__news'] = $handler;

  return $export;
}
