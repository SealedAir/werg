<?php
/**
 * @file
 * pp_blocks_promoted_slider_no_results.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function pp_blocks_promoted_slider_no_results_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-promoted_slider-block_1'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'promoted_slider-block_1',
    'i18n_block_language' => array(),
    'i18n_mode' => 0,
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
      'seeportals' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seeportals',
        'weight' => 9,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
