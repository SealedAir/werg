<?php
/**
 * @file
 * pp_page_manager_events_node_view.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function pp_page_manager_events_node_view_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__specials';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 2;
  $handler->conf = array(
    'title' => 'Events',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'specials',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'event' => 'event',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'ead6b8be-ad52-4974-9e47-d032bf4a4ee0';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-9eb3c4ea-a2db-42ab-89f4-5597108c8540';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_special_date';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'date_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'format_type' => 'long',
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_to' => '',
        'fromto' => 'both',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9eb3c4ea-a2db-42ab-89f4-5597108c8540';
    $display->content['new-9eb3c4ea-a2db-42ab-89f4-5597108c8540'] = $pane;
    $display->panels['center'][0] = 'new-9eb3c4ea-a2db-42ab-89f4-5597108c8540';
    $pane = new stdClass();
    $pane->pid = 'new-788ecc23-cd39-4010-aa97-a0ac9e228e78';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_special_text_line';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_plain',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '788ecc23-cd39-4010-aa97-a0ac9e228e78';
    $display->content['new-788ecc23-cd39-4010-aa97-a0ac9e228e78'] = $pane;
    $display->panels['center'][1] = 'new-788ecc23-cd39-4010-aa97-a0ac9e228e78';
    $pane = new stdClass();
    $pane->pid = 'new-b57226bc-4d00-426f-93e6-1d8e45e87199';
    $pane->panel = 'center';
    $pane->type = 'views_panes';
    $pane->subtype = 'slider_events-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'b57226bc-4d00-426f-93e6-1d8e45e87199';
    $display->content['new-b57226bc-4d00-426f-93e6-1d8e45e87199'] = $pane;
    $display->panels['center'][2] = 'new-b57226bc-4d00-426f-93e6-1d8e45e87199';
    $pane = new stdClass();
    $pane->pid = 'new-0b19b035-701a-47f6-ba7b-373c6b3bd46e';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '0b19b035-701a-47f6-ba7b-373c6b3bd46e';
    $display->content['new-0b19b035-701a-47f6-ba7b-373c6b3bd46e'] = $pane;
    $display->panels['center'][3] = 'new-0b19b035-701a-47f6-ba7b-373c6b3bd46e';
    $pane = new stdClass();
    $pane->pid = 'new-5a166e62-fcac-4458-b02b-191fbe4e032c';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_file_attachements';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'media',
      'delta_limit' => '0',
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'formatter_settings' => array(
        'field_delimiter' => '',
        'file_view_mode' => 'default',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '5a166e62-fcac-4458-b02b-191fbe4e032c';
    $display->content['new-5a166e62-fcac-4458-b02b-191fbe4e032c'] = $pane;
    $display->panels['center'][4] = 'new-5a166e62-fcac-4458-b02b-191fbe4e032c';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view__specials'] = $handler;

  return $export;
}
