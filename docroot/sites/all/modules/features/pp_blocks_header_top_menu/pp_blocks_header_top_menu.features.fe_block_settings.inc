<?php
/**
 * @file
 * pp_blocks_header_top_menu.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function pp_blocks_header_top_menu_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['menu-menu-header-top-menu'] = array(
    'cache' => -1,
    'css_class' => 'header-top-menu',
    'custom' => 0,
    'delta' => 'menu-header-top-menu',
    'i18n_block_language' => array(),
    'i18n_mode' => 0,
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
      'seeportals' => array(
        'region' => 'header_top',
        'status' => 1,
        'theme' => 'seeportals',
        'weight' => -37,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
