<?php
/**
 * @file
 * pp_page_manager_promotions.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function pp_page_manager_promotions_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'promotions';
  $page->task = 'page';
  $page->admin_title = 'Promotions';
  $page->admin_description = '';
  $page->path = 'promotions';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_promotions__panel_context_1714dfa7-c355-4749-ada8-4af4f02df207';
  $handler->task = 'page';
  $handler->subtask = 'promotions';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
          1 => 1,
          2 => 2,
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => 100,
        'width_type' => '%',
        'parent' => 'main-row',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'contact',
          1 => 'map',
        ),
        'parent' => 'main',
        'class' => '',
        'hide_empty' => 0,
      ),
      'contact' => array(
        'type' => 'region',
        'title' => 'Contact',
        'width' => '30.871841336957615',
        'width_type' => '%',
        'parent' => '1',
        'class' => '',
        'hide_empty' => 0,
      ),
      'map' => array(
        'type' => 'region',
        'title' => 'Map',
        'width' => '69.12815866304238',
        'width_type' => '%',
        'parent' => '1',
        'class' => '',
        'hide_empty' => 0,
      ),
      2 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'contact_us',
        ),
        'parent' => 'main',
        'class' => '',
        'hide_empty' => 0,
      ),
      'contact_us' => array(
        'type' => 'region',
        'title' => 'Contact us',
        'width' => 100,
        'width_type' => '%',
        'parent' => '2',
        'class' => '',
        'hide_empty' => 0,
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'contact' => NULL,
      'map' => NULL,
      'contact_us' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Promotions';
  $display->uuid = '4e30e08b-a529-43d6-af91-831b3631011d';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-4195566e-9517-4fcd-bb62-ff99246bdc0d';
    $pane->panel = 'center';
    $pane->type = 'views';
    $pane->subtype = 'sds_catalogue_text';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '1',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_3',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4195566e-9517-4fcd-bb62-ff99246bdc0d';
    $display->content['new-4195566e-9517-4fcd-bb62-ff99246bdc0d'] = $pane;
    $display->panels['center'][0] = 'new-4195566e-9517-4fcd-bb62-ff99246bdc0d';
    $pane = new stdClass();
    $pane->pid = 'new-c3f851f9-19e5-4ace-952f-30282f1cb4c8';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'block-7';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'promotions-intro',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c3f851f9-19e5-4ace-952f-30282f1cb4c8';
    $display->content['new-c3f851f9-19e5-4ace-952f-30282f1cb4c8'] = $pane;
    $display->panels['center'][1] = 'new-c3f851f9-19e5-4ace-952f-30282f1cb4c8';
    $pane = new stdClass();
    $pane->pid = 'new-03e4cd6d-5747-4cc3-b911-4abd1837d034';
    $pane->panel = 'center';
    $pane->type = 'views_panes';
    $pane->subtype = 'promotions-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'promotions-list',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '03e4cd6d-5747-4cc3-b911-4abd1837d034';
    $display->content['new-03e4cd6d-5747-4cc3-b911-4abd1837d034'] = $pane;
    $display->panels['center'][2] = 'new-03e4cd6d-5747-4cc3-b911-4abd1837d034';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-c3f851f9-19e5-4ace-952f-30282f1cb4c8';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['promotions'] = $page;

  return $pages;

}
