<?php
/**
 * @file
 * pp_wysiwyg.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function pp_wysiwyg_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'use media wysiwyg'.
  $permissions['use media wysiwyg'] = array(
    'name' => 'use media wysiwyg',
    'roles' => array(
      'Editor' => 'Editor',
      'Super-editor' => 'Super-editor',
      'administrator' => 'administrator',
    ),
    'module' => 'media_wysiwyg',
  );

  return $permissions;
}
