<?php
/**
 * @file
 * pp_views_promoted_slider.features.inc
 */

/**
 * Implements hook_views_api().
 */
function pp_views_promoted_slider_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
