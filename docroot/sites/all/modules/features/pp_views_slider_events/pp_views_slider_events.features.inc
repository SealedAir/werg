<?php
/**
 * @file
 * pp_views_slider_events.features.inc
 */

/**
 * Implements hook_views_api().
 */
function pp_views_slider_events_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
