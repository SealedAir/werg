<?php
/**
 * @file
 * pp_page_manager_promotions_node_view.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pp_page_manager_promotions_node_view_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}
