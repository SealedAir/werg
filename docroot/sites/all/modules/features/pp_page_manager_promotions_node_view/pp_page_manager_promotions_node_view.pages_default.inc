<?php
/**
 * @file
 * pp_page_manager_promotions_node_view.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function pp_page_manager_promotions_node_view_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__promotions';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Promotions',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'promotions',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'promotion' => 'promotion',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'f5bda192-b0b8-4dad-bf69-58c332130f7f';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-981e7ba8-0357-419a-8162-121b4679a72b';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_market_sectors';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'inline',
      'formatter' => 'taxonomy_term_reference_plain',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '981e7ba8-0357-419a-8162-121b4679a72b';
    $display->content['new-981e7ba8-0357-419a-8162-121b4679a72b'] = $pane;
    $display->panels['center'][0] = 'new-981e7ba8-0357-419a-8162-121b4679a72b';
    $pane = new stdClass();
    $pane->pid = 'new-0d2602e7-250e-4692-8e71-c798bd6851bc';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_summary_or_trimmed',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'trim_length' => '200',
        'trim_ellipsis' => 1,
        'effect' => 'slide',
        'trigger_expanded_label' => 'Expand',
        'trigger_collapsed_label' => '',
        'trigger_classes' => 'button',
        'inline' => 1,
        'css3' => 1,
        'js_duration' => '500',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '0d2602e7-250e-4692-8e71-c798bd6851bc';
    $display->content['new-0d2602e7-250e-4692-8e71-c798bd6851bc'] = $pane;
    $display->panels['center'][1] = 'new-0d2602e7-250e-4692-8e71-c798bd6851bc';
    $pane = new stdClass();
    $pane->pid = 'new-991e9259-cc00-4319-bbc7-f2a7f4a90e36';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_intro_description';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '991e9259-cc00-4319-bbc7-f2a7f4a90e36';
    $display->content['new-991e9259-cc00-4319-bbc7-f2a7f4a90e36'] = $pane;
    $display->panels['center'][2] = 'new-991e9259-cc00-4319-bbc7-f2a7f4a90e36';
    $pane = new stdClass();
    $pane->pid = 'new-0b5016a4-ee9f-4b6c-8ef7-a1d6aa7bc67f';
    $pane->panel = 'center';
    $pane->type = 'views';
    $pane->subtype = 'slider_promotions';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'default',
      'context' => array(
        0 => '',
      ),
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '0b5016a4-ee9f-4b6c-8ef7-a1d6aa7bc67f';
    $display->content['new-0b5016a4-ee9f-4b6c-8ef7-a1d6aa7bc67f'] = $pane;
    $display->panels['center'][3] = 'new-0b5016a4-ee9f-4b6c-8ef7-a1d6aa7bc67f';
    $pane = new stdClass();
    $pane->pid = 'new-58ae9a8d-142b-4144-8116-330ec67623a7';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '58ae9a8d-142b-4144-8116-330ec67623a7';
    $display->content['new-58ae9a8d-142b-4144-8116-330ec67623a7'] = $pane;
    $display->panels['center'][4] = 'new-58ae9a8d-142b-4144-8116-330ec67623a7';
    $pane = new stdClass();
    $pane->pid = 'new-170ce8c6-aecd-46e6-9371-e496723c11c5';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_file_attachements';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'media',
      'delta_limit' => '0',
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'formatter_settings' => array(
        'field_delimiter' => '',
        'file_view_mode' => 'default',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = '170ce8c6-aecd-46e6-9371-e496723c11c5';
    $display->content['new-170ce8c6-aecd-46e6-9371-e496723c11c5'] = $pane;
    $display->panels['center'][5] = 'new-170ce8c6-aecd-46e6-9371-e496723c11c5';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view__promotions'] = $handler;

  return $export;
}
