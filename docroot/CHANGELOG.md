Changelog
---------
This changelog was initiated on 4 July 2016. Previous versions untracked.
The basics of the changelog follow semantic versioning.
**[major number].[minor number].[deployment]**

**7.44.3**
- Fix minor CSS issues on the main menu and homepage paddings

**7.44.2**
- Fix Google map pointer location from the homepage block
- Update Partner content type

**7.44.1**
- The installation profile includes Drupal core 7.44
